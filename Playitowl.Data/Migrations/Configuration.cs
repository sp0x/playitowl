using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Playitowl.Models;

namespace Playitowl.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Playitowl.Data.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Playitowl.Data.AppContext context)
        {
            if (!context.Roles.Any(r => r.Name == "AppAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var userRole = new IdentityRole { Name = "AppUser" };
                var adminRole = new IdentityRole { Name = "AppAdmin" };
                manager.Create(userRole);
                manager.Create(adminRole);
            }
            var username = "administrator@admin.co";
            var password = "Averylongpass0!";
            if (!context.Users.Any(u => u.UserName == username))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                
                var user = new ApplicationUser
                {
                    UserName = username,
                    Email = username,
                    RegisteredOn = DateTime.Now,
                    ModifiedOn = DateTime.Now
                };

                manager.Create(user, password);
                manager.AddToRole(user.Id, "AppAdmin");
            }
        }
    }
}
