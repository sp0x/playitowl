﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Playitowl.Models.Repl
{
    public class RepLoop
    {
        #region Properties and variables
        private State mState;
        public ConcurrentDictionary<Trigger, List<ITriggeredAction>> Trigs { get; private set; }
        protected ConcurrentStack<FixedArray<Func<State, Object>>> Pool
        {
            get; private set;
        }

        public int BlockThreadSize
        {
            get; private set;
        }
        public ConcurrentDictionary<Trigger, List<ITriggeredAction>> GetTriggers()
        {
            lock (Trigs)
            {
                return Trigs;
            }
        }

        public State CurrentState
        {
            get
            {
                if (mState == null) return GetGlobalState();
                else return mState;
            }
            private set { mState = value; }
        }

        public RepLoopType Type { get; private set; }
        #endregion



        #region Construction

        //Should be private, because all loops are managed by the factory, later on, session/parallel manager

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="threadedTicker"></param>
        /// <param name="parallelThreads"></param>
        private RepLoop(RepLoopType type, bool threadedTicker = true, int parallelThreads = 0)
        {
            this.Type = type;
            this.BlockThreadSize = parallelThreads;
            Trigs = new ConcurrentDictionary<Trigger, List<ITriggeredAction>>();
            Pool = new ConcurrentStack<FixedArray<Func<State, Object>>>();

            if (threadedTicker) StartTickingThread();
            else StartTicking();
        }
        #endregion

        #region REPL

        /// <summary>
        /// 
        /// </summary>
        private void StartTicking()
        {
            do
            {
                //Halt only on special occations
                Tick();
            } while (true);
        }

        /// <summary>
        /// 
        /// </summary>
        private void StartTickingThread()
        {
            var ticker = new Task(StartTicking);
            ticker.Start();
        }

        private void Tick()
        {
            switch (Type)
            {
                //If this is a spooler
                case RepLoopType.OrderedParallelBlock:
                    FixedArray<Func<State, Object>> block = null;
                    if (Pool.TryPop(out block) && block != null)
                    {
                        Parallel.ForEach(block.Items, (f) =>
                        {
                            try
                            {
                                f?.Invoke(CurrentState);
                            }
                            catch (Exception e)
                            {
                                e = e;
                            }
                        });
                    }
                    break;

                default:
                    List<List<ITriggeredAction>> fireables;
                    if (GetTriggerables(out fireables))
                    {
                        Eval(fireables);
                    }
                    break;
            }

        }
        #endregion


        #region Standart methods

        /// <summary>
        /// Assign a new action to the pool
        /// </summary>
        /// <typeparam name="TTriggeredAction"></typeparam>
        /// <param name="runOnce"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public TTriggeredAction Assign<TTriggeredAction>(bool runOnce = false, params Object[] parameters)
            where TTriggeredAction : TriggeredAction
        {
            var argumentTypes = parameters.Select(p => p.GetType()).ToArray();
            var ctor = typeof(TTriggeredAction).GetConstructor(argumentTypes);
            TTriggeredAction x = (TTriggeredAction)ctor.Invoke(parameters);
            x.Once(runOnce);
            return x;
        }
        ///Assign the action with it's trigger, so it can be fired by the reploop
        public void Assign(Trigger t, ITriggeredAction action)
        {
            lock (Trigs)
            {
                if (Trigs.ContainsKey(t)) Trigs[t].Add(action);
                else Trigs[t] = new List<ITriggeredAction>() { action };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        public void Push<T, TResult>(Func<State, TResult> action, Action<TResult> continueWith)
            where T : IPooledAction
        {
            if (this.Type != RepLoopType.OrderedParallelBlock)
            {
                throw new InvalidOperationException("REPL is not of pooled block type");
            }
            FixedArray<Func<State, Object>> lastBlock = null;
            //If last block has free threads available
            if (Pool.TryPeek(out lastBlock) && !lastBlock.IsFull)
            {
                lastBlock.Add((State state) =>
                {
                    var res = action(state);
                    continueWith(res);
                    return res;
                });
            }
            else
            {
                //Last block does not have enough free threads
                var block = new FixedArray<Func<State, Object>>(BlockThreadSize);
                block.Add((State state) =>
                {
                    var result = action(state);
                    continueWith(result);
                    return result;
                });
                Pool.Push(block);
            }
        }

        public static State GetGlobalState()
        {
            return new State();
        }
        #endregion

        #region Action and trigger handling
        private bool GetTriggerables(out List<List<ITriggeredAction>> fireable)
        {
            fireable = new List<List<ITriggeredAction>>();
            var validRun = true;
            var Trigs = GetTriggers();
            foreach (var trigger in Trigs.Keys)
            {
                if (trigger.Tester(CurrentState))
                {
                    fireable.Add(Trigs[trigger]);
                }
            }
            return validRun;
        }


        // Evaluates all needed actions
        private bool Eval(List<List<ITriggeredAction>> readyTriggers = null)
        {
            //Evented loops should not be manualy evaluated!
            if (Type == RepLoopType.Evented) return false;

            List<List<ITriggeredAction>> actions = readyTriggers;
            if ((actions == null || actions.Count == 0))
            {
                if (!GetTriggerables(out actions)) return false;
            }
            Action<List<List<ITriggeredAction>>> invoker = null;

            switch (Type)
            {
                case RepLoopType.OrderedBlock:
                    invoker = (trigeredActions) =>
                    {
                        for (int i = 0; i < trigeredActions.Count; i++)
                        {
                            for (int iAction = 0; iAction < trigeredActions[i].Count; iAction++)
                            {
                                trigeredActions[i][iAction].Invoke(CurrentState);
                                Cleanup((TriggeredAction)trigeredActions[i][iAction]);
                            }
                        }
                    };
                    break;
                case RepLoopType.Async:
                    invoker = (triggeredActions) =>
                    {
                        //Wait for all?
                        triggeredActions.AsParallel().ForAll(actionGroup =>
                        {
                            for (int iAction = 0; iAction < actionGroup.Count; iAction++)
                            {
                                actionGroup[iAction].Invoke(CurrentState);
                                Cleanup((TriggeredAction)actionGroup[iAction]);
                            }
                        });
                    };
                    break;
            }
            invoker?.Invoke(actions);
            return true;
        }


        private void Cleanup(TriggeredAction action)
        {
            //If no triggers are present or action can be executed more than once (it does not need cleaning up)
            if (Trigs == null) return;
            //Find the trigger which fired the action, and removed the action
            if (action.ExecuteOnce)
            {
                var firstOrDefault = Trigs.Keys.FirstOrDefault(trig =>
                {
                    return trig.Guid == action.TriggerId;
                });
                if (firstOrDefault != null)
                {
                    Trigs[firstOrDefault].Remove(action);
                }
            }

        }
        #endregion



        public class Factory
        {
            public static List<RepLoop> Loops { get; private set; }
            public static RepLoop Global { get; private set; }

            static Factory()
            {
                Global = new RepLoop(RepLoopType.Async);
                Loops = new List<RepLoop>();
            }

            public static RepLoop SetGlobalType(RepLoopType type)
            {
                Global.Type = type;
                return Global;
            }

            public static RepLoop Create(RepLoopType type, int parallelCount = 0)
            {
                var x = new RepLoop(type, true, parallelCount);
                Loops.Add(x);
                return x;
            }

            ///Assign a triggerable action to the loop
            public static void Assign(Trigger t, ITriggeredAction action)
            {
                Global.Assign(t, action);
            }
        }



    }
}
