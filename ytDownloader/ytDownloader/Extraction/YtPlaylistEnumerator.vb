﻿Imports System.Text.RegularExpressions
Imports System.Net
Imports System.Security.Cryptography
Imports System.Web
Imports CsQuery
Imports Fizzler.Systems.HtmlAgilityPack
Imports HtmlAgilityPack
Imports netvoidc
Imports nvoid.db.Extensions
Imports nvoid.db
Imports ytDownloader.Util

Namespace Extraction
    Public Class YtPlaylistEnumerator
        Implements IEnumerator(Of YtVideo)

#Region "Properties"
        Public Property Id As String
        Public Property OriginalUrl As String
        'Public Property NextVideoId As String
        Public Property NextVideosQue As New Queue(Of YtVideo)
        Private Property Cookies As New CookieContainer
        Public Property Index As Int32 = 0
        Public Property DownloadOptions As DownloadOptions
#End Region

#Region "Events"
        Public Event Moved(sender As YtPlaylistEnumerator, position As Int64, video As YtVideo)
        Public Event ResolvedPlaylist(name As String, author As String)
#End Region

#Region "Provates"
        Private _ytvCurrent As YtVideo
        Private _ytuDecoder As New YtUrlDecoder
        Private Property PlaylistItemContainer As Regex = New Regex(Regex.Escape("<li class=""yt-uix-scroller-scroll-unit"), RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Private Property ListDomItems As New List(Of StringIndex)
#End Region

#Region "Regex"
        Public Shared ReadOnly RxList As Regex = New Regex("(list=)(.*?)(&|$)", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Public Shared ReadOnly RxVideoId As New Regex("(watch\?v=)(.*?)(&|$)", RegexOptions.Compiled Or RegexOptions.IgnoreCase)
        Public Shared ReadOnly RxVideoTitle As New Regex("(?<=data-video-title="").*?(?="")", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Public Shared ReadOnly RxVideoThumb As New Regex("(?<=data-thumb="").*?(?="")", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
#End Region


        Public Sub New(url As String, Optional downloadOptionsBuilder As DownloadOptions = Nothing)
            YtUrlDecoder.TryNormalizeYoutubeUrl(url)
            Id = RxList.MatchGroupValue(url, 2)
            If Not String.IsNullOrEmpty(Id) Then
                If String.IsNullOrEmpty(Id) Then
                    Throw New InvalidOperationException("Could not parse url, please check if it has list=? in it!")
                End If
                OriginalUrl = url
                DownloadOptions = downloadOptionsBuilder
            End If
        End Sub



        ''' <summary>
        ''' Iterates through each video. Caches with a range of 25(defined by youtube) so it doesn't do double requests.
        ''' Each video is just a new video with it's ID.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext
            If String.IsNullOrEmpty(OriginalUrl) Then Return False
            Index += 1
            Dim videoSrc As String
            Dim crVideo As YtVideo
            'First video, and no others have been loaded
            If Index = 1 And NextVideosQue.Count = 0 Then
                If String.IsNullOrEmpty(OriginalUrl)
                    OriginalUrl = OriginalUrl
                End If
                videoSrc = WebRequestHelper.Fetch(OriginalUrl, Cookies)
                ResolvePlaylistInformation(videoSrc)
                crVideo = New YtVideo(RxVideoId.MatchGroupValue(OriginalUrl, 2))
            Else 'On next video
                If NextVideosQue.Count = 0 Then Return False
                crVideo = NextVideosQue.Dequeue()
                If NextVideosQue.Count = 0 Then
                    'Next videos needs to be fetched
                    Dim tmpUrl As String = GetVideoListUrl(Id, crVideo.Id, Index)
                    videoSrc = WebRequestHelper.Fetch(tmpUrl, Cookies)
                End If
            End If
            'Next videoId must be kept

            If String.IsNullOrEmpty(videoSrc) And NextVideosQue.Count = 0 Then
                Throw New NullReferenceException("Could not fetch URL's content!")
            ElseIf Not String.IsNullOrEmpty(videoSrc) Then
                If (YtUrlDecoder.IsVideoUnavailable(videoSrc)) Then Throw New VideoNotAvailableException()
                If NextVideosQue.Count = 0 Then
                    LastVideoId = crVideo.Id
                    If Index > 1 Then
                        LastVideoIndex = Index - 2
                    End If
                    ExtractNextVIds(videoSrc)
                End If
            End If


            If NextVideosQue.Count = 0 Then
                Return False
            Else
                _ytvCurrent = New YtVideo(crVideo.Id, False, videoSrc) '
                If Not String.IsNullOrEmpty(crVideo.Name) Then _ytvCurrent.Name = crVideo.Name
                If Not String.IsNullOrEmpty(crVideo.Author) Then _ytvCurrent.Author = crVideo.Author
                If Not String.IsNullOrEmpty(crVideo.Description) Then _ytvCurrent.Description = crVideo.Description
                If Not String.IsNullOrEmpty(crVideo.Thumb) Then _ytvCurrent.Thumb = crVideo.Thumb
                RaiseEvent Moved(Me, Index, Current)
                Return True
            End If
        End Function
        Private Property LastVideoId As String
        Private Property LastVideoIndex As UInt64
        ''' <summary>
        ''' Resolves details for the playlist, included in the source of it`s page.
        ''' </summary>
        ''' <param name="pageSource"></param>
        Private Sub ResolvePlaylistInformation(pageSource As String)
            Dim htmlDoc As CQ = pageSource 
            Dim playlistTitle = htmlDoc(".playlist-info .playlist-title")
            Dim playlistAuthor = htmlDoc(".playlist-info .author-attribution")
            Dim titleNode As IDomObject = playlistTitle.FirstOrDefault()
            Dim authoreNode As IDomObject = playlistAuthor.FirstOrDefault()


            Dim strPlaylistName As String = String.Empty
            Dim strPlaylistAuthor As String = String.Empty
            If titleNode IsNot Nothing
                strPlaylistName = titleNode.InnerText.ToString().Trim().TrimStart(vbLf).TrimEnd(vbLf)
            End If
            If authoreNode IsNot Nothing
                strPlaylistAuthor = authoreNode.InnerText.ToString().Trim().TrimStart(vbLf).TrimEnd(vbLf)
            End If

            RaiseEvent ResolvedPlaylist(strPlaylistName, strPlaylistAuthor)
        End Sub

        ''' <summary>
        ''' Gets the page for the playlist and resolves details.
        ''' </summary>
        Public Sub Fetch()
            Dim videoSrc = WebRequestHelper.Fetch(OriginalUrl, Cookies)
            ResolvePlaylistInformation(videoSrc)
        End Sub


        Private Sub ExtractNextVIds(videoSrc As String)
            '            Dim rxNextVideo As Regex
            '            Dim ixTmp As Int32 = Index
            ListDomItems = StringUtils.IndexesOfRxEx(videoSrc,PlaylistItemContainer)
            Dim plsPage As CQ = videoSrc
            Dim playlistItems = plsPage("li.yt-uix-scroller-scroll-unit")
            For Each videoNode In playlistItems
                Dim nodecq  as New CQ(videoNode)
                Dim author = nodecq.Attr("data-video-username")
                Dim linkNode = nodecq.Find("a.playlist-video").Get(0)
                Dim title = nodecq.Attr("data-video-title") 'untitled
                Dim plId = Integer.Parse(nodecq.Attr("data-index") )'0
                Dim description = "" 
                dim elipsis = (New CQ(linkNode)).Find("h4.yt-ui-ellipsis")
                If not (elipsis Is Nothing) then description = New CQ(elipsis).Text()
                'This is an old video that was parsed
                If LastVideoIndex <> 0 And plId <= LastVideoIndex Then Continue For

                Dim id = String.Empty
                If linkNode IsNot Nothing Then
                    Dim attr  = linkNode.GetAttribute("href")
                    id = YtVideo.GetVideoId(String.Format("http://youtube.com{0}", attr))
                End If
                Dim video As New YtVideo(id)
                If Not String.IsNullOrEmpty(author) then video.Author = author
                If Not String.IsNullOrEmpty(title) Then video.Name = title
                If Not String.IsNullOrEmpty(description) Then video.Description = description
                NextVideosQue.Enqueue(video)
            Next

            '            '<li class=""yt-uix-scroller-scroll-unit'
            '            rxNextVideo = GenNextVideoRegex(Id, Index)
            '            NextVideoId = rxNextVideo.MatchGroupValue(videoSrc, 2)
            '            Dim tmpNextVideoId As String = NextVideoId
            '            ''Builds a stack of the next visible URL's
            '            Do
            '                If(ListDomItems.Count<(ixTmp)) Then Exit Do 
            '                Dim item = ListDomItems(ixTmp-1)
            '                Dim itemEnding = videoSrc.IndexOf("</li>", item.Value)
            '                dim videoHtmlItem = videoSrc.Substring(item.Value, itemEnding - item.Value)
            '                dim title = videoHtmlItem.Rx(RxVideoTitle)
            '                Dim thumb = videoHtmlItem.Rx(RxVideoThumb)
            '                Dim tmpVideo = New YtVideo(tmpNextVideoId)
            '                If title IsNot Nothing And title.Count > 0 Then tmpVideo.Name = title(0)
            '                If thumb IsNot Nothing And thumb.Count > 0 Then tmpVideo.Thumb = thumb(0)
            '                NextVideosQue.Enqueue(tmpVideo)
            '                ixTmp += 1
            '                rxNextVideo = GenNextVideoRegex(Id, ixTmp)
            '                tmpNextVideoId = rxNextVideo.MatchGroupValue(videoSrc, 2)
            '            Loop While Not String.IsNullOrEmpty(tmpNextVideoId)
        End Sub

        Public Sub Reset() Implements IEnumerator.Reset
            Index = 1
        End Sub

#Region "Current"
        Public ReadOnly Property Current() As YtVideo Implements IEnumerator(Of YtVideo).Current
            Get
                Return _ytvCurrent
            End Get
        End Property
        Public ReadOnly Property IEnumerator_Current() As Object Implements IEnumerator.Current
            Get
                Return _ytvCurrent
            End Get
        End Property
#End Region

#Region "IDispose"
        Public Sub Dispose() Implements IDisposable.Dispose
            Id = Nothing
        End Sub
#End Region

#Region "Helpers"
        Public Shared Function GetVideoListUrl(listId As String, videoId As String, index As Int32)
            Return String.Format("https://www.youtube.com/watch?v={0}&list={1}&index={2}", videoId, listId, index)
        End Function
        Private Shared Function GenNextVideoRegex(listId As String, cIndex As Int32)
            Const template As String = "(watch\?v=)(.*)((&amp;list={0}&amp;index={1}"")|(&amp;index={1}&amp;list={0}""))"
            Return New Regex(String.Format(template, listId, cIndex + 1), RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        End Function
#End Region


    End Class
End Namespace