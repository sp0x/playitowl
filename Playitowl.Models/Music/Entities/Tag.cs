﻿using System;
using System.Xml.Serialization;

using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("tag", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class Tag : MusicalEntity
    {
        /// <summary>
        /// Gets or sets the count of times this tag has been referenced!
        /// </summary>
        [XmlAttribute("count")]
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        public override string EntityType => "tag";
        public override Type WrapperType => null;
        public override string Query { get; set; }
        public override Type ListType => typeof (TagList);

        public override string ToString()
        {
            return Name;
        }
    }
}
