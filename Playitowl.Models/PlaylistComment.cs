namespace Playitowl.Models
{
    public class PlaylistComment : Comment
    {
        public virtual Playlist Playlist { get; set; }
    }
}