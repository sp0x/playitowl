﻿var PLAYLIST_TYPE_POPULAR = 'popular';
var PLAYLIST_TYPE_GENRES = 'genres';
var PLAYLIST_TYPE_EXPLORE = 'explore';


/**
 * 
 * 
 * @returns {} 
 */
var Playlist = function (ops) {
    var self = this;
    this.options = {
        id : (ops && ops.id!=undefined) ? ops.id : null,
        search: (ops && ops.search != undefined) ? ops.search : false,
        details : (ops && ops.details!=undefined) ? ops.details : false,
    }
    /**
     * 
     * 
     */
    this.controls = {
        PlaylistContainer: $('.playlist-container'),
        TabSelectors : $('.playlist .tab')
    };
    /**
     * 
     * 
     */
    this.context = {
        currentOffset: 0,
        currentType: PLAYLIST_TYPE_POPULAR,
    }
    /**
     * 
     * 
     */
    this.templates = {
        playlistItem: null,
        getPlaylistItem : function(cbk) {
            if (self.templates.playlistItem == null) {
                $.get('/Playlist/GetItemTemplate').done(function(response) {
                    var compiledItemTemplate = _.template(response);
                    self.templates.playlistItem = compiledItemTemplate;
                    if(cbk) cbk(null, self.templates.playlistItem);
                });
            } else {
                if(cbk) cbk(null, self.templates.playlistItem);
                return self.templates.playlistItem;
            }
        }
    }


    /**
     *  
     * @returns {} 
     */
    this.init = function () { 
        this.initTemplates().then(function () {
            self.setupHandlers();
            if (!(self.options.search)) {
                self.populate(PLAYLIST_TYPE_POPULAR);
            }
            


        }).catch(function () {
            console.log("Could not load templates!");
        }); 
    } 
    this.setupHandlers = function () {
        var mc = this.controls;
        var self = this;
        mc.TabSelectors.click(self.onTabClicked);
        if (self.options && self.options.search === true) {
            self.handleSearchEvents();
        }
        if (self.options && self.options.details === true) {
            self.handlePlaylistDetailsEvents();

        }

    }
    this.handleSearchEvents = function() {
        var $results = $('.playlist-search-container div.pl-item');
        $results.click(self.handlePlaylistClick);
    }
    this.handlePlaylistDetailsEvents = function() {
        var $videos = $('.playlist-details-container .pl-video');
        $videos.click(self.handleVideoClick);
        var $btnDownload = $('.btn-pl-download');
        $btnDownload.click(self.onPlaylistDownload);
        var $btnDownloadSide = $('.side.main .menu .side-download');
        $btnDownloadSide.click(self.onPlaylistDownload);



    }
    this.onPlaylistDownload = function () {
        var url = "/Playlist/Download/" + self.options.id;
        //console.log(url);
        //Site.DownloadFile(url);
    }

    this.clearPlaylists = function() {
        self.controls.PlaylistContainer.empty();
    }
    /**
     * 
     * 
     * @returns {} 
     */
    this.populate = function (type) {
        if (type == undefined) type = PLAYLIST_TYPE_POPULAR;
        if (type !== this.context.currentType) {
            //Changing the type
            self.resetPlaylistContext(type); 
        } else this.context.currentType = type;
        //Clear previous playlist content.
        self.clearPlaylists(); 
        $.ajax({
            type: "POST",
            url: '/Playlist/Fetch',
            data: {
                type: type,
                offset: this.context.currentOffset,
                count: 25,
            },
            success: function(e) {
                e = JSON.parse(e);
                console.log(e);
                self.onPopulateContent(self, e);
            }
        }); 

    }
    /**
     * Resets the playlist information.
     * Use this when switching from one playlist type, to a different one.
     * @param {} type 
     * @returns {} 
     */
    this.resetPlaylistContext = function (type) { 
        self.context.currentOffset = 0;
        self.context.currentType = type;
    }
    /**
     * Parse the fetched playlists, and fill templates for them, and populate the main playlist container.
     * @param {} content 
     * @returns {} 
     */
    this.onPopulateContent = function (self, content) {
        //Set the current index
        self.context.currentOffset =0;
        _(content).forEach(function (playlistItem) {
            var item = $(self.templates.playlistItem(playlistItem)); 
            self.controls.PlaylistContainer.append(item);
            item.click(self.handlePlaylistClick);
        });

    }

    /**
     * Handles a video click
     * @returns {} 
     */
    this.handleVideoClick = function() {
        var item = $(this);
        var vid = item.attr('id');
        var $x = $('<a href="https://youtube.com/watch?v=' + vid + '" target="_blank"></a>');
        $(document.body).append($x);
        $x[0].click();
        $x.remove();
    }

    this.handlePlaylistClick = function() {
        var item = $(this);
        var plid = item.attr('id');
        var pname = encodeURIComponent($(item.find('.pl-name')).text()); 
        var plink = '/Playlist/v/' + pname + '/' + plid;
        //console.log(item, plid);
        window.location = plink; 
    }
    /**
     *  
     * @returns {} 
     */
    this.initTemplates = function () {
        //TODO: Implement a callback queue here, in order for multiple templates to be loaded, before callback invocation
        var self = this;
        return new Promise(function (resolve, reject) {
            var getPlaylistItem = Promise.promisify(self.templates.getPlaylistItem);
            getPlaylistItem().then(resolve)
            .catch(function () {
                console.log("Template error: getPlaylistItem");
                reject();
            });
        });
    }
    this.onTabClicked = function () {
        var type = $(this).attr('type');
        switch (type) {
            case PLAYLIST_TYPE_POPULAR:
                break;
            case PLAYLIST_TYPE_GENRES:
                break;
            case PLAYLIST_TYPE_EXPLORE:
                break;
        }
        self.populate(type);
    }

} 
Playlist.prototype.constructor = Playlist;
