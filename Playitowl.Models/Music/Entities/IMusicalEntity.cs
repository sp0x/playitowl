﻿using System;

namespace Playitowl.Models.Music.Entities
{
    /// <summary>
    /// Standart musical entity having a type, id, query
    /// </summary>
    public interface IMusicalEntity
    {
        string EntityType { get; }
        string Id { get; set; }
        string Query { get; set; }
        Type WrapperType { get; }
    }
}