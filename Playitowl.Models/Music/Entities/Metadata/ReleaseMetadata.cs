﻿using System;
using System.Xml.Serialization;
using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities.Metadata
{
    [XmlRoot("metadata", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class ReleaseMetadata : MetaListWrapper<ReleaseList>, IMusicalEntity
    {
        /// <summary>
        /// Gets or sets the release-list collection.
        /// </summary>
        [XmlElement("release-list")]
        public override ReleaseList Collection { get; set; }

        public override string EntityType => "metadata"; 
        public override string Query { get; set; }
        public override Type WrapperType { get; }
    }
}
