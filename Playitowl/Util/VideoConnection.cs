﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Fleck;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using Playitowl.Data.Repositories;
using Playitowl.Models;
using Playitowl.Service;
using ytDownloader;
using ytDownloader.Extraction;
//using nvoid.db.DB.RDS;
//using nvoid.db.Extensions;

namespace Playitowl.Util
{
    public class VideoConnection : SessionAction
    {
        public string OutputDirectory { get; private set; } = Path.Combine(HttpRuntime.AppDomainAppPath, "downloads");
        public VideoWrapper CurrentParsingVideo { get; set; }
        
        public IWebSocketConnection Socket { get; set; }
        public VideoConnectionState State { get; private set; }

        private bool ShouldDisplayPlaylistInfo { get; set; } = true;
        //        public static RemoteDataSource<Playlist> PlaylistProvider { get; set; } = typeof(Playlist).GetDataSource<Playlist>();
        //        public static RemoteDataSource<YtVideo> VideoProvider { get; set; } = typeof(YtVideo).GetDataSource<YtVideo>(); 

        //Dim usersrc As RemoteDataSource = GetType(User).GetDataSource() ' : usersrc.Trash()
        //Dim categ As RemoteDataSource = GetType(Category).GetDataSource() '  : categ.Trash()
        //Dim artsrc As RemoteDataSource = GetType(Article).GetDataSource() '  : artsrc.Trash()
        //Dim corssrc As RemoteDataSource = GetType(Course).GetDataSource() ' : corssrc.Trash()
        private IVideoService _videoService;
        private IPlaylistService _playlistService;
        private string _userId;
        private ApplicationUser _user;
        private ApplicationUserManager _userManager;
        private double _updateDelta = 0;
        public double UpdateInterval = 2d;
        public Stopwatch Timer = new Stopwatch();
        public UInt64 DownloadCounter { get; private set; }
        public String TargetLink { get; private set; }

        private Downloader LastDownloader { get; set; }

        private string lastAction;

        public VideoConnection()
        {
            _videoService = new VideoService(new VideoRepository());
            _playlistService = new PlaylistService(new PlaylistRepository(), new VideoRepository());
            var userStore = new UserStore<ApplicationUser>(); 
            _userManager = new ApplicationUserManager(userStore);
        }

        public VideoConnection(IWebSocketConnection socket) : this()
        {
            this.Socket = socket;
            string path = socket.ConnectionInfo.Path;
            string userId = null;
            if (!string.IsNullOrEmpty(path))
            {
                var query = path;
                if (path.Contains("?")) query = path.Substring(path.IndexOf("?") + 1);
                userId = HttpUtility.ParseQueryString(query).Get("id");
            }
            if (!string.IsNullOrEmpty(userId))
            {
                SetUserId(userId);
            }
            socket.OnMessage = message =>
            {
                try
                {
                    Object result = handleMessage(message);
                    if (result != null) socket.Send(JsonConvert.SerializeObject(result));
                }
                catch (Exception e)
                {
                    socket.Send(JsonConvert.SerializeObject(new
                    {
                        Status = "Error",
                        Exception = e.Message
                    }));
                }
            };
        }


        public void WriteLine(String message)
        {
            if (Socket == null) return;
            Object payload = new {message = $"{message}\n" };
            Socket.Send(JsonConvert.SerializeObject(payload));
        }

        
        /// <summary>
        /// Returns an action which waits(ASYNC) for the client to reply and invokes a callback
        /// </summary>
        /// <param name="message">The message to write</param>
        /// <returns></returns>
        public Action<Action> WriteReply(LinkedAction message)
       {
            var action = Listeners.Add(message);
            Write(message);
            return action;
       }
        public void Write(Object message)
        {
            if (Socket == null) return;
            Socket.Send(JsonConvert.SerializeObject(message));
        }
        public void Write(String message)
        {
            if (Socket == null) return;
            Object payload = new { message = $"{message}" };
            Socket.Send(JsonConvert.SerializeObject(payload));
        }

         

        /// <summary>
        /// 
        /// </summary>
        /// <param name="videoIdOrUrl"></param>
        /// <param name="q"></param>
        /// <param name="f"></param>
        /// <param name="onlyV"></param>
        /// <returns></returns>
        private Object Prepare(String videoIdOrUrl, String q, String f, Boolean onlyV)
        {
            Object result = null;
            if (videoIdOrUrl == null)
                throw new ArgumentException("Id or url should be passed in the id field, when using action prepare.");
            this.CurrentParsingVideo = new ytDownloader.VideoWrapper(videoIdOrUrl);
            this.TargetLink = videoIdOrUrl; 
            this.CurrentParsingVideo.setOptions(q, f, onlyV);
            this.State = VideoConnectionState.Prepared;
            result = new { success = true , isPlaylist = YtPlaylistEnumerable.IsPlaylist(videoIdOrUrl) }; //"{ 'success' : true }";
            return result;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="addPlaylistInfo"></param>
        /// <returns></returns>
        private object Download(Boolean addPlaylistInfo = false)
        {
            Object result = null;
            if (this.State >= VideoConnectionState.Prepared)
            {
                this.CurrentParsingVideo = this.CurrentParsingVideo;
                Action<Downloader> executor = LaunchDownload;
                String quality = "";
                Downloader.Factory.TryParseQuality(ref quality);
                CurrentParsingVideo.setQuality(quality);
                //Set the output path, to the current application`s output directory
                CurrentParsingVideo.Options.OutputPath = OutputDirectory;
                CurrentParsingVideo.Options.OnlyVideo = false;

                ShouldDisplayPlaylistInfo = addPlaylistInfo; 

                if (true)//!ops.IsInformationRequest)
                {
                    Downloader.Factory.PlaylistResolved += HandlePlaylistResolved;
                    Downloader.Factory.VideoResolved += HandleVideoResolved;
                    Downloader.Factory.ExecuteList(TargetLink , CurrentParsingVideo, executor, onNextVideoLoading);
                }
            }
            else
                throw new InvalidOperationException(
                    "CurrentParsingVideo connection must be prepared first. Please call act:prepare");
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="playlistEnumerable"></param>
        private void HandlePlaylistResolved(YtPlaylistEnumerable playlistEnumerable)
        {
            Downloader.Factory.PlaylistResolved -= HandlePlaylistResolved;
            var task = new Task(()=>
            {
                var model = new Playlist(playlistEnumerable)
                {
                    Url = playlistEnumerable.Url
                };
                if (model.Videos == null) model.Videos = new List<Video>();
                //_videoService.SavePlaylist(playlistEnumerable);
                _playlistService.SavePlaylist(model);
                var videos = new List<Video>();
                try
                {
                    foreach (YtVideo yVideo in playlistEnumerable)
                    {
                        var video = AutoMapper.Mapper.Map<Video>(yVideo);
                        video.Playlist = model;
                        _videoService.SaveVideo(video);
                        videos.Add(video);
                    }
                    _playlistService.SetPlaylistVideos(model, videos);
                }
                catch (Exception ex)
                {
                    ex = ex;
                }
            });
            task.Start();
//            PlaylistProvider.Persist(model);
//            VideoProvider.PersistAll(model.Videos.ToList());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="yvideo"></param>
        private void HandleVideoResolved(YtVideo yvideo)
        { 
            Video newVideo = AutoMapper.Mapper.Map<Video>(yvideo);
            _videoService.SaveVideo(newVideo);
        }

        /// <summary>
        /// Handles playlist video resolving, in order to synchronize 
        /// </summary>
        /// <param name="video"></param>
        /// <returns></returns>
        private Action<Action> onNextVideoLoading(YtVideo video)
        {
            if (Socket == null || video == null) return null ;
            //Notify that the next video is ready to be loaded, and when the client replies, ACK it's loading.
            this.CurrentParsingVideo = new ytDownloader.VideoWrapper(video);
            var action = new LinkedAction("loadingPlaylistEntry") { Id = video.Id };
            return WriteReply(action);

        }



        private string localizePath(string Path)
        {
            if (String.IsNullOrEmpty(Path)) return Path;
            return Path = Path.Replace(HttpRuntime.AppDomainAppPath, "");
        }
        /// <summary>
        /// Handles socket messages
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private Object handleMessage(String message)
        {
            Object result = null;
            dynamic json = JsonConvert.DeserializeObject(message);
            if (json.action == null) return null; 
            if (json.action == "prepare")
            {
                String q = json.quality == null ? null : json.quality.ToString();
                String f = json.format == null ? "mp3" : json.format.ToString();
                Boolean onlyV = json.onlyVideo == null ? false : json.onlyVideo == "true";
                if (onlyV && f == "mp3") f = null;
                //Id might be an id or an url(most likely)
                result = Prepare(json.id.ToString(), q, f ,onlyV );
            }
            //ACTION : ACK, replyId : Id of the stored action callback
            else if (json.action == "ack" && json.replyId != null)
            {
                Listeners.Raise(json.replyId.ToString());
                result = null;
            }
            else if (json.action == "download")
            {
                lastAction = json.action;
                bool? addInfo = json.addPlaylistInfo;
                result = Download(addInfo==null ? false : addInfo.Value);
            }
            else if (json.action == "ok" && lastAction == "download")
            {
                resumeLastDownloader();
            }
            return result;
        }




        public enum VideoConnectionState
        {
            Empty, Failed, Preparing, Prepared, Downloading, Completed
        } 

        /// <summary>
        /// Handles the launch of downloads
        /// </summary>
        /// <param name="downloader"></param>
        public void LaunchDownload(Downloader downloader)
        {
            try
            {
                downloader = Downloader.Initialize(ref downloader, x => x.Id);
                downloader.Paths["video_storage"] = Path.Combine(HttpRuntime.AppDomainAppPath , "storage\\tmpvideo");
                String extension = Path.GetExtension(downloader.OutputPath);

                downloader.OutputPath = downloader.OutputPath.Substring(0, downloader.OutputPath.Length - extension.Length);
                downloader.OutputPath = $"{downloader.OutputPath}{extension}";

                Trace.WriteLine(CurrentParsingVideo.ToString());
                String type = "";
                if (downloader is AudioDownloader)  type = "audio"; 
                else  type = "video"; 
                Write(new
                {
                    action = "ready",
                    resultType = type,
                    playlistMember = downloader.IsPlaylistMember,
                    id = downloader.CurrentVideo.Id
                });
                downloader.ExtractionProgressChanged += updateHandler;
                downloader.DownloadProgressChanged += updateHandler;
                downloader.DownloadFinished += DownloadFinished;
                //PrintStatement("To", dldr.OutputPath);
                LastDownloader = downloader;
                LastDownloader.Start();
                DownloadCounter += 1;
            }
            catch (Exception ex)
            {
               WriteLine($"Exception: {ex.Message}");
            }
        }

        private void resumeLastDownloader()
        {
            if (LastDownloader == null) return;
            LastDownloader.Start();
            DownloadCounter += 1;
        }

        private void DownloadFinished(object sender, IoFinishedEventArgs eventData)
        {
            
            Timer.Stop();
            string id = null;
            if (sender.GetType().IsSubclassOf(typeof(Downloader)))
            {
                var dldr = (Downloader)sender;
                id = dldr.CurrentVideo?.Id;
            }
            if (id == null) id = CurrentParsingVideo.Id;
            switch (eventData.Mode)
            {
                case IoMode.File:
                    String filename = Path.GetFileName(eventData.Path); 
                    String safePath = "/Downloads/" + filename;

                   // WriteLine(string.Format("Finished [in {0}] downloading and saved to {1}", Timer.Elapsed, e.Path));
                    Write(new
                    {
                        action = "finished" ,
                        time = Timer.Elapsed,
                        path = safePath,
                        id = id
                    }); 
                    Downloader downloader = (Downloader)sender;
                    if (downloader.CurrentVideo != null && downloader.CurrentVideo.InnerItems != null && downloader.CurrentVideo.InnerItems.Keys.Count>0)
                    {
                        var items = downloader.CurrentVideo.InnerItems.Select(item => $"{item.Value}//spl__it//{item.Key}").ToList();
                        var cutQuery = new { file = localizePath(eventData.Path),  action = "query" , type = "cut", items = items};
                        Write(cutQuery); //Handle it later on!
//                        if ((downloader.CurrentVideo.InnerItems.Count > 0))
//                        {
//                            string cutIt = Console.ReadLine().ToLower();
//                            if ((cutIt.StartsWith("y") | cutIt.StartsWith("yes")))
//                            {
//                                MP3Cutter cutter = new MP3Cutter(e.Path, System.IO.Path.GetDirectoryName(e.Path));
//                                cutter.applyTimeDictionary(downloader.CurrentVideo.InnerItems);
//                            }
//                        }

                    }

                    break;
                case IoMode.Streaming:

                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateHandler(object sender, ProgressEventArgs e)
        {
            Action tsAction = () =>
            {
                string str = null;
                bool shouldPrint = false;
                double delta = Math.Abs(e.ProgressPercentage - _updateDelta);
                if (_updateDelta.Equals(0m) | _updateDelta.Equals(100m))
                {
                    shouldPrint = true;
                    _updateDelta = e.ProgressPercentage;
                    // get delta 
                }
                else if (delta > UpdateInterval)
                {
                    shouldPrint = true;
                    _updateDelta = e.ProgressPercentage;
                }

                if (shouldPrint)
                { 
                    str = string.Format("{0:F2}", e.ProgressPercentage);  
                    Write(new {action = "update", completion = str});
                }
            };
            Task.Factory.StartNew(tsAction);
        }
         

        /// <summary>
        /// Console logging.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        private void PrintStatement(string a, string b)
        {
            string str = string.Format("{0}: ", a);
            Console.ForegroundColor = ConsoleColor.Green;
            Write(str);
            str = string.Format("{0}", b);
            Console.ForegroundColor = ConsoleColor.Red;
            WriteLine(str);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private async Task<ApplicationUser> GetUser()
        {
            if (_user != null) return _user;
            if (!string.IsNullOrEmpty(_userId))
            {
                _user = await _userManager.FindByIdAsync(_userId);
                return _user;
            }
            else
            {
                return null;
            }
        }
        public void SetUserId(string userId)
        { 
            _userId = userId;
        }
    }

  
}