﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization; 

namespace Playitowl.Models.Music.Entities.Collections
{ 
    /// <summary>
    /// Basic list which only needs T to be overriten, and is a base for lists which are also entities.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseList<T> : IMusicalEntityCollection
        where T : Entity
    {


        #region Props
        [XmlIgnore]
        public abstract List<T> Items { get; set; }
        /// <summary>
        /// Gets or sets the total list items count.
        /// </summary>
        /// <remarks>
        /// This might be different form the actual list items count. If the list was
        /// generated from a search request, this property will return the total number
        /// of available items (on the server), while the number of returned items is
        /// limited by the requests 'limit' parameter (default = 25).
        /// </remarks>
        [XmlAttribute("count")]
        public int QueryCount { get; set; }

        /// <summary>
        /// Gets or sets the list offset (only available in search requests).
        /// </summary>
        [XmlAttribute("offset")]
        public int QueryOffset { get; set; }
        public int Count => QueryCount;
        public object SyncRoot { get; }
        public bool IsSynchronized { get; }
        T this[int index]
        {
            get { return Items[index]; }
            set { Items[index] = value; }
        }

        public bool IsReadOnly => false;
        public bool IsFixedSize => false;
        #endregion


        #region Construction
        //private List<T> buffer;
        public BaseList()
        {
            Items = new List<T>();
        }

        public BaseList(IEnumerable<T> items)
        {
            Items = new List<T>(items);
        }

        public BaseList(IEnumerable items)
        {
            Items = new List<T>(items.Cast<T>());
        }

        #endregion


        #region Methods

        IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        public IEnumerator GetEnumeratorBase()
        {
            return Items.GetEnumerator();
        }  

        public bool Remove(T item)
        {
            return Items.Remove(item);
        }

        public void Add(T item)
        {
            Items.Add(item);
        }

        public void Clear()
        {
            this.Items.Clear();
        }

        public bool Contains(T item)
        {
            return Items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Items.CopyTo(array, arrayIndex);
        } 
         
        public void Remove(object value)
        {
            Items.Remove((T)value);
        }

        public int IndexOf(T item)
        {
            return Items.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            Items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Items.RemoveAt(index);
        }
        public IEnumerable getItems()
        {
            return Items;
        }

        public object FirstOrDefault()
        {
            throw new NotImplementedException();
        }
        #endregion



    }
}