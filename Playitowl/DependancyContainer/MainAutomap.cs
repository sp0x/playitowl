using System.Linq;
using AutoMapper;
using Playitowl.Models;
using Playitowl.Models.Vm;
using ytDownloader.Extraction;

namespace Playitowl.DependancyContainer
{
    public class MainAutomap
    {
        public MainAutomap()
        {
        }

        public static void Load()
        {
            Mapper.Initialize(m =>
            {
                m.CreateMap<YtVideo, Video>(); 
                m.CreateMap<Video, YtVideo>(); 
                m.CreateMap<Video, VideoVm>()
                    .ForMember(dest => dest.CoverUrl, opts => opts.MapFrom(src => src.GetCoverUrl()))
                    .ForMember(dest => dest.Thumbnail, opts => opts.MapFrom(src => Mapper.Map<YtVideo>(src).GetThumbnail()));
                m.CreateMap<Playlist, PlaylistVm>()
                    .ForMember(dest => dest.Cover, opts => opts.MapFrom(src => src.GetCover()))
                    .ForMember(dest => dest.Items,
                        opts => opts.MapFrom(src => src.Videos.Select(vid => Mapper.Map<VideoVm>(vid))));
            });
        }
    }
}