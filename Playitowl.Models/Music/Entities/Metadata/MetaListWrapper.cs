﻿using System;

namespace Playitowl.Models.Music.Entities.Metadata
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MetaListWrapper<T> : MetadataWrapper<T>, IMusicalEntity
        where T : IMusicalEntityCollection
    { 
        public override Type ListType => typeof (T);
    }
}