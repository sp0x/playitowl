using System;
using System.ComponentModel.DataAnnotations;

namespace Playitowl.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public virtual ApplicationUser Author { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "From")]
        public DateTime CreatedOn { get; set; }
        [Required]
        public String Content { get; set; }
        public CommentType Type { get; set; }
    }
}