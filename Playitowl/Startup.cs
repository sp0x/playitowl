﻿using System;
using System.Collections.Generic;
using System.Web;
using Fleck;
using Microsoft.Owin;
using Owin; 
using Playitowl.DependancyContainer;
using Playitowl.Util;

[assembly: OwinStartupAttribute(typeof(Playitowl.Startup))]
namespace Playitowl
{
    public partial class Startup
    {
        public static List<VideoConnection> VideoConnections { get; set; }
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app); 
            MainAutomap.Load();
            var wserver = new WebSocketServer("ws://0.0.0.0:8080");
            var rootPath = HttpRuntime.AppDomainAppPath;
            //rootPath = System.IO.Path.Combine(rootPath, "keys\\playitowl.web.pfx"); 
            //wserver.Certificate = certificate;
            wserver.Start(socket =>
            {
                socket.OnOpen = () => Console.WriteLine("Open!");
                socket.OnClose = () => Console.WriteLine("Close!");
                if (VideoConnections == null) VideoConnections = new List<VideoConnection>();
                var videoConnection = new VideoConnection(socket); 
                VideoConnections.Add(videoConnection);
            });
        }
    }
}
