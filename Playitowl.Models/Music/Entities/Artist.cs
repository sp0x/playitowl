﻿using System;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Playitowl.Models.Music.Entities.Collections;
using Playitowl.Models.Music.Entities.Metadata;

namespace Playitowl.Models.Music.Entities
{
    //http://musicbrainz.org/ns/mmd-2.0#
    [XmlRoot("artist", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class Artist : MusicalEntity
    {
        public const string EntityName = "artist";

        #region Properties

        /// <summary>
        /// Gets or sets the score (only available in search results).
        /// </summary>
        [XmlAttribute("score", Namespace = "http://musicbrainz.org/ns/ext#-2.0")]
        public int Score { get; set; }

        public override Type ListType => typeof (ArtistList);

        /// <summary>
        /// Gets or sets the MusicBrainz id.
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [XmlAttribute("type")]
        public string Type { get; set; }

        public override string EntityType => "artist";
        public override Type WrapperType => typeof(ArtistMetadata);
        public override string Query { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the sort name.
        /// </summary>
        [XmlElement("sort-name")]
        public string SortName { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        [XmlElement("gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the life-span.
        /// </summary>
        [XmlElement("life-span")]
        public LifeSpanNode LifeSpan { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [XmlElement("country")]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the disambiguation.
        /// </summary>
        [XmlElement("disambiguation")]
        public string Disambiguation { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        [XmlElement("rating")]
        public Rating Rating { get; set; }

        #endregion

        #region Subqueries

        [XmlElement("recording-list")]
        public RecordingList Recordings { get; set; }

        [XmlElement("release-group-list")]
        public ReleaseGroupList ReleaseGroups { get; set; }

        [XmlElement("release-list")]
        public ReleaseList ReleaseLists { get; set; }

        [XmlElement("work-list")]
        public WorkList Works { get; set; }

        [XmlElement("tag-list")]
        public TagList Tags { get; set; }


        #endregion

        public Artist()
        {
        }
        public Artist(String name)
        {
            this.Name = name;
            this.Query = name;
        }

        #region Static Methods

        [Obsolete("Use LookupEntity() method.")]
        public static Artist Get(string id, params string[] inc)
        {
            return (Artist)LookupEntity<Artist>(EntityName, id, inc).Result;
        }

//        [Obsolete("Use GetAll() method.")]
//        public ArtistList Search(int limit = 25, int offset = 0)
//        {
//            return GetAll<ArtistMetadata>(this, limit, offset).Result.Collection;
//        }

        [Obsolete("Use GetRelated() method.")]
        public ArtistList Browse(string relatedEntity, string value, int limit = 25, int offset = 0, params  string[] inc)
        {
            return (ArtistList)BrowseAsync<Artist>(this, relatedEntity, value, limit, offset, inc).Result;
        }



        /// <summary>
        /// Lookup an artist in the MusicBrainz database.
        /// </summary>
        /// <param name="id">The artist MusicBrainz id.</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        public async Task<Artist> GetAsync(string id, params string[] inc)
        {
            return (await LookupEntity<Artist>(EntityName, id, inc));
        }

        #endregion

        public override string ToString()
        {
            return this.Name;
        }
    }

    #region Include entities

    #endregion

}
