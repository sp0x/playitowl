﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Playitowl;
using Playitowl.Areas.Main.Controllers;
using Playitowl.Data.Repositories;
using Playitowl.Service;

namespace Playitowl.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            var videoRepository = new VideoRepository();
            var playlistService = new PlaylistService(new PlaylistRepository(), videoRepository);
            var videoService = new VideoService(videoRepository);
            HomeController controller = new HomeController(playlistService, videoService);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            var videoRepository = new VideoRepository();
            var playlistService = new PlaylistService(new PlaylistRepository(), videoRepository);
            var videoService = new VideoService(videoRepository);
            // Arrange
            HomeController controller = new HomeController(playlistService, videoService);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            var videoRepository = new VideoRepository();
            var playlistService = new PlaylistService(new PlaylistRepository(), videoRepository);
            var videoService = new VideoService(videoRepository);
            HomeController controller = new HomeController(playlistService, videoService);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
