﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Playitowl.Models;

namespace Playitowl.Data.Repositories
{
    public interface IVideoRepository
    {
        IQueryable<Video> AsQueryable();
        Video GetById(string id);
        void Create(Video yvideo);
        void Remove(string id);
        IEnumerable<Video> OrderByDescending<TMember>(Expression<Func<Video, TMember>> p0);
        IEnumerable<Video> OrderBy<TMember>(Expression<Func<Video, TMember>> p0);
    }
}