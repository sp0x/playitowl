﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks; 

using Playitowl.Models.Music.Entities;
using ytDownloader.Extraction;
//
//using nvoid.db;
//using nvoid.db.DB.Patterns;
//using nvoid.db.Extensions;

namespace Playitowl.Models
{
    /// <summary>
    /// 
    /// </summary> 
    public class Playlist 
    {
        //private string mId; 
        #region Fields
        private string mCreatorName;
        private string mName;
        private YtPlaylistEnumerable _mPlaylistEnumerable;
        #endregion

        #region Properties
        public string Id { get; set; }
        public int Hits { get; set; }
        public string Url { get; set; }
        public virtual ICollection<Video> Videos { get; set; }
        public string Author { get; set; }
        public virtual ApplicationUser LocalProfile { get; set; }
        public virtual ICollection<PlaylistComment> Comments { get; set; }

        public string Name { get; set; }
        public String SearchTerm { get; set; }
        public List<string> Genre { get; set; }
        public DateTime CreatedOn { get; set; }

        //        public YtPlaylist Videos
        //        {
        //            get
        //            {
        //                 return mPlaylist ?? (mPlaylist = new YtPlaylist(Id));
        //            }
        //            set
        //            {
        //                mPlaylist = value;
        //            }
        //        }

        //        public string CreatorName
        //        {
        //            get
        //            {
        //                return mCreatorName ?? (mCreatorName = mPlaylist?.CreatorName ?? string.Empty);
        //            }
        //            set
        //            {
        //                mCreatorName = value;
        //            }
        //        }


        #region Construction

        public Playlist()
        {

        }

        public Playlist(YtPlaylistEnumerable playlistEnumerable) : this()
        {
            this._mPlaylistEnumerable = playlistEnumerable;
            Id = playlistEnumerable.Id;
            this.Author = playlistEnumerable.CreatorName;
            this.Name = playlistEnumerable.Name;
        }
        public Playlist(ApplicationUser creator) : this()
        {
            this.LocalProfile = creator;
            this.Author = creator.UserName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="playlistId">The id of the playlist or a full link</param>
        public Playlist(string playlistId, ApplicationUser creator = null, bool fetchDetails = false)
        {
            this.Id = playlistId;
            if (creator != null)
            {
                LocalProfile = creator;
                Author = creator.UserName;
            }
            _mPlaylistEnumerable = new YtPlaylistEnumerable(playlistId, null, fetchDetails);
        }
        #endregion


        public YtPlaylistEnumerable AsEnumerable()
        {
            if(_mPlaylistEnumerable==null)
                _mPlaylistEnumerable = new YtPlaylistEnumerable(Id);
            return _mPlaylistEnumerable;
        }
//        public string ListId
//        {
//            get { return mId; }
//            set
//            {
//                if (value == null)
//                {
//                    return;
//                }
//                mId = YtPlaylist.GetListId(value);
//            }
//        }

        #endregion


        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<String>> ResolveCategory()
        {
            var genre = new List<string>();
            //Save each song's genre and sum the genres, the first 3 only are accounted!
            var genresDict = new Dictionary<string, UInt32>();
            var i = 0;
            foreach (var video in Videos)
            {
                Artist artist = null;
                //Parse track artist name, from the recording(song)
                Recording.FromTitle(video.Name, out artist);
                if (artist != null)
                {
                    var tx = (Artist)await artist.FindFirst();
                    if (tx == null) continue;
                    //Get the most used tags, matching the genres filter
                    var artistGenre = tx.Tags?.GetMostReferencedTag(PlaylistHelper.ValidGenres);
                    if (artistGenre == null) continue;
                    Trace.WriteLine($"{i++} - {artistGenre.Name}");
                    if (genresDict.ContainsKey(artistGenre.Name))
                    {
                        genresDict[artistGenre.Name]++;
                    }
                    else
                    {
                        genresDict.Add(artistGenre.Name,0);
                    }
                }
                //For each video, query it's title in the music db and resolve it, cache it if needed
                 
            } 
            genre = genresDict.OrderBy(x => x.Value).Take(3).Select(x=> x.Key).ToList();
            this.Genre = genre;
            return genre;
        } 

        #region Implementations

        public string GetCover()
        {
            var firstOrDefault = Videos?.FirstOrDefault();
            if (firstOrDefault == null) return null;
            YtVideo ytVid = AutoMapper.Mapper.Map<YtVideo>(firstOrDefault);
            return ytVid.GetThumbnail();
        }
        /// <summary>
        /// Gets the Hits, Videos, Genre, ListId, CreatedOn, Creator
        /// </summary>
        /// <returns></returns>
        public Object Representation()
        {
            var firstOrDefault = Videos?.FirstOrDefault();
            if (firstOrDefault == null) return null;
            YtVideo ytVid = AutoMapper.Mapper.Map<YtVideo>(firstOrDefault);
            var Cover = ytVid.GetThumbnail() ?? ""; 
            return new
            {
                Id, Hits, Videos, Genre, CreatedOn, Cover, Author, Name
            };
        }
        #endregion

        #endregion
    }
}