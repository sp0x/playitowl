﻿namespace Playitowl.Models
{
    public enum PlaylistType
    {
        Explore,
        Popular,
        Genres
    }

    public static class PlaylistTypeExtensions
    {
        public static PlaylistType ToPlaylistType(this string type)
        {
            if (type.ToLower() == PlaylistType.Explore.ToString().ToLower()) return PlaylistType.Explore;
            else if(type.ToLower() == PlaylistType.Genres.ToString().ToLower()) return PlaylistType.Genres;
            else if(type.ToLower() == PlaylistType.Popular.ToString().ToLower()) return PlaylistType.Popular;
            return PlaylistType.Popular;
        }
    }
}