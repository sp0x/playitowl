﻿using System;
using System.Xml.Serialization;

using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("work", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class Work : MusicalEntity
    {
        public override string EntityType => "work";
        public override Type WrapperType { get; }
        public override string Query { get; set; }

        public override Type ListType => typeof (WorkList);

        /// <summary>
        /// Gets or sets the MusicBrainz id.
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [XmlElement("title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the ISW code.
        /// </summary>
        [XmlElement("iswc")]
        public string ISWC { get; set; }
    }
}
