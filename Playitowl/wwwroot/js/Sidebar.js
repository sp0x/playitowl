﻿var Sidebar = function() {
    var self = this;
    this.controls = {
        ItemList: $('.sidebar-list'),
        SideMenuTrigger: $('.side.main.menu'),
        Body: $('.body-wrap')
    };
    this.initialize();
}

Sidebar.prototype = {
    /**
     * Wether or not, the current page has any sidebar items.
     * @returns {} 
     */
    hasItems: function () { return this.controls.ItemList.children().length > 0; },
    /**
     * 
     * @returns {} 
     */
    initialize : function() {
        if (this.hasItems()) {
            if (this.controls.SideMenuTrigger.length === 0) {
                var menuTrigger = $('<div class="menu main side">' +
                    '<span class="glyphicon glyphicon-menu-hamburger"></span>' +
                    '</div>');
                this.controls.Body.prepend(menuTrigger);
            }
        }
    }
}
Sidebar.prototype.constructor = Sidebar;