using System;

namespace Playitowl.Models.Repl
{
    /// <summary>
    /// An action that is triggered in a given state
    /// </summary>
    public interface ITriggeredAction
    {
        void Invoke(State state);
        Guid TriggerId { get; }
    }
}