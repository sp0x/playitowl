﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("release-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class ReleaseList : MusicalEntityList<Release>
    {
        /// <summary>
        /// Gets or sets the list of releases.
        /// </summary>
        [XmlElement("release")]
        public override List<Release> Items { get; set; }

        public override string EntityType => "release-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
