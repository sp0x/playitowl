﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization; 

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("artist-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class ArtistList : MusicalEntityList<Artist>
    {
        /// <summary>
        /// Gets or sets the list of artists.
        /// </summary>
        [XmlElement("artist")]
        public override List<Artist> Items { get; set; }  
        public override string EntityType => "artist-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
