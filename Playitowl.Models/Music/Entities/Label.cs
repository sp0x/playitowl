﻿using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("label")]
    public class Label
    {
        /// <summary>
        /// Gets or sets the MusicBrainz id.
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
    }
}
