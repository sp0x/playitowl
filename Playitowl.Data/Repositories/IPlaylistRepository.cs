﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Playitowl.Models;

namespace Playitowl.Data.Repositories
{
    public interface IPlaylistRepository
    {
        IQueryable<Playlist> AsQueryable();
        void Create(Playlist list);
        Playlist FindById(string listId);
        IEnumerable<Playlist> OrderByDescending<TMember>(Expression<Func<Playlist, TMember>> p0);
        IEnumerable<Playlist> OrderBy<TMember>(Expression<Func<Playlist, TMember>> p0);
        void Remove(string id);
        void SetPlaylistVideos(Playlist model, ICollection<Video> videos);
    }
}