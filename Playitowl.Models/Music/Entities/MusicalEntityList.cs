using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities
{
    /// <summary>
    /// A standart entity representing a list of entities, of various types
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MusicalEntityList<T> : BaseList<T>, IMusicalEntity
        where T : MusicalEntity
    {  
        /// <summary>
        /// 
        /// </summary>
        public abstract string EntityType { get; }
        public abstract string Id { get; set; }
        public abstract string Query { get; set; }
        /// <summary>
        /// The metadata wrapper type, which wraps this list
        /// </summary>
        public abstract Type WrapperType { get; }

        public MusicalEntityList() : base()
        {
        }

        public MusicalEntityList(IEnumerable<T> items) : base(items)
        {
        }

        public MusicalEntityList(IEnumerable items) : base(items)
        {
            
        }

        public object FirstOrDefault()
        {
            return Items.FirstOrDefault();
        }

        public IEnumerator GetEnumerator()
        {
            return GetEnumeratorBase();
        }
    }

}