﻿using System;
using System.Collections.Generic;

namespace playitowl.web.Music
{
    public enum TypeCategory
    {
        Lister, MetaWrapper, Entity
    }

    public class TypeDict : Dictionary<Type, TypeLib>
    {
        public static TypeDict Instance { get; set; }

        static TypeDict()
        {
            Instance = new TypeDict();
        }

        public TypeLib this[Type key]
        {
            get
            {
                if (!base.ContainsKey(key))
                {
                    base[key] = new TypeLib();
                }
                return base[key];
            }
        }
    }
    public class TypeLib 
    {
        public Dictionary<TypeCategory, Type> TypeMaps { get; private set; }

        #region Construction
        public TypeLib()
        {
            TypeMaps = new Dictionary<TypeCategory, Type>();
        }
        #endregion
        
        public TypeLib Map(TypeCategory type, Type t)
        {
            if (this.TypeMaps.ContainsKey(type)) TypeMaps[type] = t; 
            else TypeMaps.Add(type, t);
            return this;
        }

        public Type this[TypeCategory key]
        {
            get
            {
                if (TypeMaps.Count == 0)
                {
                    TypeMaps = TypeMaps;
                }
                return TypeMaps[key];
            }
        }
    }
}