﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Fleck.Helpers;
using Newtonsoft.Json;
using Playitowl.Models;
using Playitowl.Models.Vm;
using Playitowl.Service; 
//using nvoid.db;
//using nvoid.db.DB; 
//using nvoid.db.DB.RDS;
//using nvoid.db.Extensions;
//using nvoid.extensions.Extensions;


namespace Playitowl.Areas.Main.Controllers
{
    public class PlaylistController : Controller
    { 
        private IPlaylistService _playlistService;
        private IVideoService _videoService;
        public PlaylistController(IPlaylistService playlistService,
            IVideoService videoService) : base()
        {
            this._playlistService = playlistService;
            this._videoService = videoService;
        }

        /// <summary>
        /// GET: Playlist
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// POST: Playlist/Fetch
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Fetch()
        {
            string type = Request["type"];
            int offset = 0;
            int count = 25;
            if (!int.TryParse(Request["offset"], out offset)) offset = 0;
            if (!int.TryParse(Request["count"], out count)) count = 25;
            List<Playlist> fetchByType = _playlistService.FetchByType(type.ToPlaylistType(), count, offset).ToList();
            List<PlaylistVm> models = AutoMapper.Mapper.Map<List<Playlist>, List<PlaylistVm>>(fetchByType);
            var result = JsonConvert.SerializeObject(models);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Route("Playlist/v/{name}/{id}")]
        public ActionResult ViewPlaylist(String name, string id)
        {
            Playlist targetPlaylist = _playlistService.GetById(id);
            if (targetPlaylist != null) targetPlaylist.Name = HttpUtility.UrlDecode(targetPlaylist.Name);
            PlaylistVm vmodel = Mapper.Map<PlaylistVm>(targetPlaylist);
            return View("Playlist", vmodel);
        }

        /// <summary>
        /// GET: Playlist/GetItemTemplate
        /// Get a simple playlist item template.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetItemTemplate()
        {
            return PartialView("PlaylistItem");
        }


        /// <summary>
        /// Collects all existing videos which are assigned to the playlist with the given id. 
        /// Builds a ZIP archive with all of the files, and returns it in the responding request.
        /// </summary>
        /// <param name="id">The `ListID` of the playlist to download</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Download(string id)
        {
            var list =_playlistService.GetById(id);
            string xzy = Request["id"];
            if (list == null) return View("Error", new HandleErrorInfo(new Exception("Non-existant playlist ID!"), "Playlist", "Download"));
            var pathDownloads = Path.Combine(HttpRuntime.AppDomainAppPath, "downloads");
            var pathArchives = Path.Combine(HttpRuntime.AppDomainAppPath, "archives");
            var nameBase64 = (DateTime.Now).ToString("yyyyMMddHHmmssffff");
            var pathArchive = Path.Combine(pathArchives, $"{nameBase64}_{id}.zip");

            if (!Directory.Exists(pathArchives)) Directory.CreateDirectory(pathArchives);

            var files = new List<string>();
            foreach (var video in list.Videos)
            {
                if (video == null) continue; 
                files.AddRange(Directory.GetFiles(pathDownloads, $"{video.Id}.mp3"));
            }
            if (!System.IO.File.Exists(pathArchive))
            {
                var zipPlaylist = new Archiver(files.ToArray());
                zipPlaylist.setBasePath(pathDownloads);
                zipPlaylist.CreatePackage(pathArchive, x =>
                {
                    //Rename zipped filenames to be to the corresponding video name.
                    var ext = Path.GetExtension(x);
                    var name = x.Substring(0, x.Length - ext.Length);
                    var video = list.Videos.FirstOrDefault(v => v.Id == name);
                    if (!ext.StartsWith(".")) ext = "." + ext;
                    if (video != null)
                    {
                        return string.Concat(video.Name, ext);
                    }
                    else return string.Concat(name, ext);
                });
            }
            
             
            var finfo = new FileInfo(pathArchive);
            string downloadFilename = ((list != null) ? list.Name : id) + ".zip";
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", HttpHelper.GetDisposition(Request, downloadFilename));
            Response.AddHeader("Content-Length", finfo.Length.ToString());
            Response.ContentType = "binary/octed-stream";
            Response.Flush();
            Response.TransmitFile(pathArchive);
            try
            {
                Response.End();
            }
            catch (Exception e)
            {
            
            }
            return null;
        } 

        [HttpPost]
        public ActionResult Search(PlaylistSearchBm searchModel)
        { 
            if (string.IsNullOrEmpty(searchModel.SearchTerm)) searchModel.SearchTerm = string.Empty;
            string searchTerm = searchModel.SearchTerm.ToLower();
            IEnumerable<Playlist> items = _playlistService.GetAllByPartialName(searchTerm);
            var viewItems = new PlaylistSearchVm(items);
            //var resultModel = new PlaylistCursor(cursor);
            return View("Search", viewItems);
        }
    }


}
