﻿using System.Web.Mvc; 
using Playitowl.Service; 
//using nvoid.db.DB; 
//using nvoid.db.DB.RDS;
//using nvoid.db.Extensions;

//using VideoModel = playitowl.web.Models.VideoModel;

namespace Playitowl.Areas.Main.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    { 
        private IPlaylistService _playlistService;
        private IVideoService _videoService;

        public HomeController(IPlaylistService playlistService, IVideoService videoService)
        {
            this._playlistService = playlistService;
            this._videoService = videoService;
        }
        [OutputCache(Duration = 100, VaryByParam = "none", Location = System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Duration = 100, VaryByParam = "none")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [OutputCache(Duration = 100, VaryByParam = "none")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }  
    }
}