﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Serialization;
using playitowl.web.Music;

using Playitowl.Models.Music.Entities.Metadata;
//using nvoid.db.Extensions;

namespace Playitowl.Models.Music.Entities
{
    public abstract class MusicalEntity : Entity, IMusicalEntity
    {
        internal static string WebServiceUrl = Configuration.MusicBrainzEndpoint;
        internal const string LookupTemplate = "{0}/{1}/?inc={2}";
        internal const string BrowseTemplate = "{0}?{1}={2}&limit={3}&offset={4}&inc={5}";
        internal const string SearchTemplate = "{0}?query={1}&limit={2}&offset={3}";
        

        #region Abstractions
        /// <summary>
        /// The name of the entity, used in order to escape entity name arguments.
        /// </summary>
        public abstract string EntityType { get; }
        /// <summary>
        /// If the Entity has a wrapper class, define it's type here.
        /// </summary>
        public abstract Type WrapperType { get; }
        /// <summary>
        /// Holds the query which resolved the item
        /// </summary>
        public abstract string Query { get; set; }
        public abstract Type ListType { get; }

        #endregion

        #region Properties
        [XmlAttribute("id")]
        public string Id { get; set; }
        #endregion



        public enum RequestType
        {
            Lookup, Browse, Search
        }

        internal static string CreateUrl(string entity, string query, RequestType type, string mbid, string relatedEntity, string inc,
            int limit = 25, int offset = 0)
        {
            var result = "";
            var template = "";
            switch (type)
            {
                case RequestType.Browse:
                    template = BrowseTemplate;
                    result = $"{WebServiceUrl}{String.Format(BrowseTemplate, entity, relatedEntity, mbid, limit, offset, inc)}";
                    break;
                case RequestType.Lookup:
                    template = LookupTemplate;
                    result = $"{WebServiceUrl}{String.Format(LookupTemplate, entity, mbid, inc)}";
                    break;
                case RequestType.Search:
                    template = SearchTemplate;
                    result = $"{WebServiceUrl}{String.Format(SearchTemplate, entity, Uri.EscapeUriString(query), limit, offset)}";
                    break;
                default:
                    return null;
            }
            return result;
        }
        internal string CreateUrl(RequestType type, string mbid, string relatedEntity, string inc,
            int limit = 0,int offset = 0)
        {
            return CreateUrl(EntityType, Query, type, mbid, relatedEntity, inc, limit, offset);
        }

       
        public async Task<MusicalEntity> LookupEntity<T>(params String[] inc) 
            where T : MusicalEntity
        {
            return await LookupEntity<T>(EntityType, Id , inc);
        }


        #region Fetchers

        public async Task<MusicalEntity> FindFirst()
        {
            IEnumerable enumerable = (await GetAll(this, 1, 0));
            return enumerable?.Cast<MusicalEntity>().FirstOrDefault();
        }

        #region Get All

        public async Task<IEnumerable> GetAll(int limit = 25, int offset = 0)
        {
            var res = await (GetAll(this, limit, offset));
            return res;
        }

        //        public async Task<String> GetAll<T>(int limit = 25, int offset = 0)
        //            where T : BaseList<MusicalEntity>
        //        {
        //            MetadataWrapper entity = await MusicalEntity.GetAll(this, limit, offset);
        //            return new MusicalEntityList<T>(entity.getItems()); 
        //        }
        /// <summary>
        /// Sends a search request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to search for.</param>
        /// <param name="query">The query string.</param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, default = 0).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntityList<T>> GetAll<T>(MusicalEntity entity, int limit = 25, int offset = 0)
            where T : MusicalEntity
        {
            if (entity == null) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            var searchRequest = entity.CreateUrl(RequestType.Search, null, null, null, limit, offset);
            try
            {
                return await WebRequestHelper.GetAsync<T>(searchRequest, withoutMetadata: false);
            }
            catch (Exception ex)
            { 
                return null;
            }
        }

        /// <summary>
        /// Sends a search request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to search for.</param>
        /// <param name="query">The query string.</param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, default = 0).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntityList<T>> Get<T>(MusicalEntity entity, int limit = 25, int offset = 0)
            where T : MusicalEntity
        {
            if (entity == null) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            var searchRequest = entity.CreateUrl(RequestType.Search, null, null, null, limit, offset);
            var targetType = typeof(T);
            if (entity.ListType != null) targetType = entity.ListType;
            MethodInfo method = typeof(WebRequestHelper).GetMethod("GetAsync");
            MethodInfo generic = method.MakeGenericMethod(targetType);
            MetadataWrapper<MusicalEntityList<T>> result = (MetadataWrapper<MusicalEntityList<T>>)generic.Invoke(entity, new object[] { searchRequest, false });
            return result.Collection;
        }
        /// <summary>
        /// Sends a search request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to search for.</param>
        /// <param name="query">The query string.</param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, default = 0).</param>
        /// <returns></returns>
        protected async static Task<IEnumerable> GetAll(MusicalEntity entity, int limit = 25, int offset = 0)
        {
            if (entity == null) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            var searchRequest = entity.CreateUrl(RequestType.Search, null, null, null, limit, offset);
            //{Name = "ArtistMetadata" FullName = "playitowl.web.Music.Entities.Metadata.ArtistMetadata"}
            //Name = "Task`1" FullName = "System.Threading.Tasks.Task`1[[playitowl.web.Music.Entities.MetadataWrapper`1[[playitowl.web.Music.Entities.MusicalEntityList`1[[playitowl.web.Music.Entities.Metadata.ArtistMetadata, playitowl.web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]], playitowl.web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]], playitowl.web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]"
            //Task<metadatawrapper<MusicalEntityList<ArtistMetadata>>>
            MethodInfo method = typeof(WebRequestHelper).GetMethod("GetAsync");
            MethodInfo generic = method.MakeGenericMethod(entity.GetType());//Task<MetadataWrapper<MusicalEntityList< >>>
            var resType = typeof(MusicalEntityList<>).MakeGenericType(entity.GetType());
            IMusicalEntityCollection result = await (dynamic)generic.Invoke(entity, new object[] { searchRequest, false });
            if (result == null) return null;
            return (IEnumerable)result.getItems();
            //dynamic rx = (Convert.ChangeType(result, resultType));
            //return rx.Result;
        }


        #endregion

        #endregion





        #region Browsing

        #endregion
        public async Task<MusicalEntityList<T>> BrowseAsync<T>(string relatedEntity, string relatedEntityId,
            int limit, int offset, params string[] inc) 
            where T : MusicalEntity
        {
            return await BrowseAsync<T>(this, relatedEntity, relatedEntityId, limit, offset, inc);
        }
        /// <summary>
        /// Sends a browse request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to browse.</param>
        /// <param name="relatedEntity"></param>
        /// <param name="relatedEntityId"></param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, default = 0).</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntityList<T>> BrowseAsync<T>(MusicalEntity entity,
            string relatedEntity, string relatedEntityId, int limit, int offset, params string[] inc) 
            where T : MusicalEntity
        {
            if (entity == null) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            var searchRequest = entity.CreateUrl(RequestType.Browse, relatedEntityId, relatedEntity, String.Join("+", inc), limit, offset);
            return await WebRequestHelper.GetAsync<T>(searchRequest, withoutMetadata: false);
        }

        /// <summary>
        /// Sends a lookup request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to lookup.</param>
        /// <param name="id">The MusicBrainz id of the entity.</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        protected async static Task<T> LookupEntity<T>(string entity, string id, params string[] inc) 
            where T : MusicalEntity
        {
            if (string.IsNullOrEmpty(entity)) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            if (string.IsNullOrEmpty(id)) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "id"));
            var lookupUrl = CreateUrl(entity, id, RequestType.Lookup, null, null, String.Join("+", inc));
            var res = await WebRequestHelper.GetAsync<T>(lookupUrl);
            return res.Items.FirstOrDefault();
        }











    }
}