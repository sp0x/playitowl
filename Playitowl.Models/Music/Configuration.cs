﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Playitowl.Models.Music.Entities;
using Playitowl.Models.Music.Entities.Collections;
using Playitowl.Models.Music.Entities.Metadata;

namespace playitowl.web.Music
{
    public static class Configuration
    { 
        static Configuration()
        {
            GenerateCommunicationThrow = true;
            Proxy = null;
            UserAgent = "Hqub.MusicBrainz/2.0";
            TypeDict.Instance[typeof (Artist)].Map(TypeCategory.Entity, typeof (Artist))
                .Map(TypeCategory.Lister, typeof (ArtistList))
                .Map(TypeCategory.MetaWrapper, typeof (ArtistMetadata));



        }

        /// <summary>
        /// If true, then all exceptions for http-requests to MusicBrainz (from class WebRequestHelper) will
        /// throw up. Otherwise they will be suppressed.
        /// </summary>
        public static bool GenerateCommunicationThrow { get; set; }


        /// <summary>
        /// Gets or sets a <see cref="System.Net.IWebProxy"/> used to query the webservice.
        /// </summary>
        public static IWebProxy Proxy { get; set; }

        /// <summary>
        /// Allow set cutstom user agent string.
        /// </summary>
        public static string UserAgent { get; set; }

        public static string MusicBrainzEndpoint => "http://127.0.0.1:5001/ws/2/";
    }
}
