﻿using System;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using Playitowl.Service;

namespace Playitowl.Areas.Administration.Controllers
{
    [Authorize(Roles = "AppAdmin")]
    public class PlaylistController : Controller
    {
        private IVideoService _videoService;
        private IPlaylistService _playlistService;


        public PlaylistController(IVideoService videoService, IPlaylistService plService)
        {
            _videoService = videoService;
            _playlistService = plService;
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var playlists = _playlistService.GetSorted(sortOrder);
            if (!String.IsNullOrEmpty(searchString))
            {
                playlists = playlists.Where(s => s.Name.Contains(searchString));
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(playlists.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Delete(string id)
        {
            _playlistService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}