﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization; 

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("medium-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class MediumList : MusicalEntityList<Medium>
    {
        /// <summary>
        /// Gets or sets the medium track count.
        /// </summary>
        /// <remarks>
        /// Only available in the result of a release search (???).
        /// </remarks>
        [XmlElement(ElementName = "track-count")]
        public int TrackCount { get; set; }

        /// <summary>
        /// Gets or sets the list of mediums.
        /// </summary>
        [XmlElement("medium")]
        public override List<Medium> Items { get; set; }

        public override string EntityType => "medium-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
