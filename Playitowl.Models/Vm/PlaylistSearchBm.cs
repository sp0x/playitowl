﻿using System.ComponentModel.DataAnnotations;

namespace Playitowl.Models.Vm
{
    public class PlaylistSearchBm
    {
        [Required]
        public string SearchTerm { get; set; }
    }
}