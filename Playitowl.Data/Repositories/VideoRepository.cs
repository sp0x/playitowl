﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Playitowl.Models;

namespace Playitowl.Data.Repositories
{
    public class VideoRepository : Data.Repositories.IVideoRepository
    {
        public AppContext _entities = new AppContext();

        public IQueryable<Video> AsQueryable()
        {
            return _entities.Videos.AsQueryable<Video>();
        }

        public Video GetById(string id)
        {
            return _entities.Videos.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Video yvideo)
        {
            var existingElement = _entities.Videos.FirstOrDefault(x => x.Id == yvideo.Id);
            if (existingElement == null)
            {
                _entities.Videos.Add(yvideo);
                if (yvideo.Added == DateTime.MinValue) yvideo.Added = DateTime.Now;
                if (yvideo.UploadDate == DateTime.MinValue) yvideo.UploadDate = DateTime.Now;
                var writes = _entities.SaveChanges();
            }
        }

        public void Remove(string id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                _entities.Videos.Remove(entity);
                _entities.SaveChanges();
            }
        }

        public IEnumerable<Video> OrderByDescending<TMember>(Expression<Func<Video, TMember>> p0)
        {
            return _entities.Videos.OrderByDescending(p0);
        }

        public IEnumerable<Video> OrderBy<TMember>(Expression<Func<Video, TMember>> p0)
        {
            return _entities.Videos.OrderBy(p0);
        }
    }
}