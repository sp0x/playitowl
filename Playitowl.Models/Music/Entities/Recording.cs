﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Serialization;
using playitowl.web.Music;

using Playitowl.Models.Extensions;
using Playitowl.Models.Music.Entities.Collections;
using Playitowl.Models.Music.Entities.Metadata;

//using static nvoid.db.Extensions.Extensions;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("recording", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class Recording : MusicalEntity
    {
        public const string EntityName = "recording";

        #region Properties

        /// <summary>
        /// Gets or sets the score (only available in search results).
        /// </summary>
        [XmlAttribute("score", Namespace = "http://musicbrainz.org/ns/ext#-2.0")]
        public int Score { get; set; }

        public override Type ListType => typeof (RecordingList);

        /// <summary>
        /// Gets or sets the MusicBrainz id.
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }
         
        public override string EntityType => "recording";
        public override Type WrapperType => typeof (RecordingMetadata);

        public override string Query
        {
            get; set;
        } 
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [XmlElement("title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        [XmlElement("length")]
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets the disambiguation.
        /// </summary>
        [XmlElement("disambiguation")]
        public string Disambiguation { get; set; }

        #endregion
        public static Recording FromTitle(string title, out Artist artist)
        {
            var track = new Recording();
            string delimiter = null;
            var delIndex = title.IndexOfAny(out delimiter, " - ", " -- ");
            if (delIndex >= 0)
            {
                var parts = title.Split(new string[] { delimiter }, StringSplitOptions.None );
                if (parts.Length > 0)
                {
                    artist = new Artist(parts[0]);
                    if (parts.Length > 1)
                    {
                        track.Query = parts[1];
                        track.Title = parts[1];
                        var ix = track.Query.IndexOfAny(new char[] {'(', '|'});
                        if (ix > -1)
                        {
                            track.Query = track.Query.Substring(0, ix);
                        }
                    }
                }
                else artist = null;


            }
            else artist = null;
            return track;
        }

        #region Include

        [XmlElement("tag-list")]
        public TagList Tags { get; set; }

        [XmlArray("artist-credit")]
        [XmlArrayItem("name-credit")]
        public List<NameCredit> Credits { get; set; }

        [XmlElement("release-list")]
        public ReleaseList Releases { get; set; }

        #endregion

        #region Static methods

        [Obsolete("Use LookupEntity() method.")]
        public static Recording Get(string id, params string[] inc)
        {
            return LookupEntity<Recording>(EntityName, id, inc).Result;
        }

        [Obsolete("Use GetAll() method.")]
        public RecordingList Search(string query, int limit = 25, int offset = 0)
        {
            this.Query = query;
            return (RecordingList)GetAll<Recording>(this, limit, offset).Result;
        }

        [Obsolete("Use GetRelated() method.")]
        public RecordingList Browse(string relatedEntity, string value, int limit = 25, int offset = 0, params  string[] inc)
        {
            return (RecordingList)BrowseAsync<Recording>(this, relatedEntity, value, limit, offset, inc).Result;
        }

        /// <summary>
        /// Lookup an recording in the MusicBrainz database.
        /// </summary>
        /// <param name="id">The recording MusicBrainz id.</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        public async static Task<Recording> GetAsync(string id, params string[] inc)
        {
            return (await LookupEntity<Recording>(EntityName, id, inc));
        }

        /// <summary>
        /// Search for an recording in the MusicBrainz database, matching the given query.
        /// </summary>
        /// <param name="query">The query string.</param>
        /// <param name="limit">The maximum number of recordings to return (default = 25).</param>
        /// <param name="offset">The offset to the recordings list (enables paging, default = 0).</param>
        /// <returns></returns>
        public async Task<RecordingList> SearchAsync(string query, int limit = 25, int offset = 0)
        {
            return (RecordingList)(await GetAll<Recording>(this, limit, offset));
        }

        /// <summary>
        /// Search for an recording in the MusicBrainz database, matching the given query.
        /// </summary>
        /// <param name="query">The query parameters.</param>
        /// <param name="limit">The maximum number of recordings to return (default = 25).</param>
        /// <param name="offset">The offset to the recordings list (enables paging, default = 0).</param>
        /// <returns></returns>
        public async Task<RecordingList> SearchAsync(QueryParameters<Recording> query, int limit = 25, int offset = 0)
        {
            return (RecordingList)(await GetAll<Recording>(this, limit, offset));
        }

        /// <summary>
        /// Browse all the recordings in the MusicBrainz database, which are directly linked to the
        /// entity with given id.
        /// </summary>
        /// <param name="entity">The name of the related entity.</param>
        /// <param name="id">The id of the related entity.</param>
        /// <param name="limit">The maximum number of recordings to return (default = 25).</param>
        /// <param name="offset">The offset to the recordings list (enables paging, default = 0).</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        public async Task<RecordingList> BrowseAsync(string entity, string id, int limit = 25, int offset = 0, params  string[] inc)
        {
            return (RecordingList)(await BrowseAsync<Recording>(this,entity, id, limit, offset, inc));
        }

        #endregion
    }
}
