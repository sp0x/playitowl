﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("recording-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class ReleaseGroupList : MusicalEntityList<ReleaseGroup>
    {
        /// <summary>
        /// Gets or sets the list of release-groups.
        /// </summary>
        [XmlElement("release-group")]
        public override List<ReleaseGroup> Items { get; set; }

        public override string EntityType => "release-group";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType =>null;
    }
}
