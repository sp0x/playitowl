﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Playitowl.Models;

namespace Playitowl.Data.Repositories
{
    public class PlaylistRepository : Data.Repositories.IPlaylistRepository
    {
        public AppContext _entities = new AppContext();


        public IQueryable<Playlist> AsQueryable()
        {
            return _entities.Playlists.AsQueryable<Playlist>();
        }

        public void Create(Playlist list)
        {
            if (list.CreatedOn == DateTime.MinValue) list.CreatedOn = DateTime.Now; 
            _entities.Playlists.Add(list);
            var writes = _entities.SaveChanges();
            writes = writes;
        }

        public Playlist FindById(string listId)
        {
            return _entities.Playlists.FirstOrDefault(x => x.Id == listId);
        }



        public IEnumerable<Playlist> OrderByDescending<TMember>(Expression<Func<Playlist, TMember>> p0)
        {
            return _entities.Playlists.OrderByDescending(p0);
        }

        public IEnumerable<Playlist> OrderBy<TMember>(Expression<Func<Playlist, TMember>> p0)
        {
            return _entities.Playlists.OrderBy(p0);
        }

        public void Remove(string id)
        {
            var playlist = FindById(id);
            if (playlist != null)
            {
                _entities.Playlists.Remove(playlist);
                _entities.SaveChanges();
            }
        }

        public void SetPlaylistVideos(Playlist model, ICollection<Video> videos)
        {
            _entities.Playlists.Attach(model);
            model.Videos = videos;
            var entry = _entities.Entry(model);
            entry.Property(x => x.Videos).IsModified = true;
            _entities.SaveChanges();
        }
    }
}