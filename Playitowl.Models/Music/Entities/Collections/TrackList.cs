﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("track-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class TrackList :  MusicalEntityList<Track>
    {
        /// <summary>
        /// Gets or sets the list of tracks.
        /// </summary>
        [XmlElement("track")]
        public override List<Track> Items { get; set; }

        public override string EntityType => "track-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
