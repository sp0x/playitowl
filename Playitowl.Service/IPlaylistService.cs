using System.Collections.Generic;
using Playitowl.Models;

namespace Playitowl.Service
{
    public interface IPlaylistService
    {
        void Create(Playlist video);
        IEnumerable<Playlist> ListVideos();
        Playlist GetByVideoId(string id);
        IEnumerable<Playlist> FetchByType(PlaylistType toPlaylistType, int count, int offset);
        Playlist GetById(string id);
        IEnumerable<Playlist> GetAllByPartialName(string searchTerm);
        void SavePlaylist(Playlist model);
        IEnumerable<Playlist> GetSorted(string sortOrder);
        void Remove(string id);
        void SetPlaylistVideos(Playlist model, ICollection<Video> videos);
    }
}