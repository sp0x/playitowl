﻿using System.ComponentModel.DataAnnotations;

namespace Playitowl.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }


    }
}