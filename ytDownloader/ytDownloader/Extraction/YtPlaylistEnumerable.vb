Imports System.Globalization
Imports Fizzler.Systems.HtmlAgilityPack
Imports HtmlAgilityPack
Imports netvoidc.DB
Imports nvoid.db.DB

Namespace Extraction
    Public Class YtPlaylistEnumerable
        Implements IEnumerable(Of YtVideo)
        Public Id As String
        Private Property CachedItems As List(Of YtVideo)
        Private Property CacheIndex As Int64
        Public Property Name As String
        Public Property CreatorName As String
        Public Property Url As String
        Public Property DownloadOptions As DownloadOptions

        Public Sub New()
        End Sub

        Public Sub New(plUrl As String, Optional dldOps As DownloadOptions = Nothing, Optional fetchDetails As Boolean = False)
            Url = plUrl
            DownloadOptions = dldOps
            Me.Id = GetListId(Url)
            If fetchDetails Then
                Dim ytpEnumerator = New YtPlaylistEnumerator(Url, DownloadOptions)
                AddHandler ytpEnumerator.ResolvedPlaylist, AddressOf HandlePlaylistResolution
                ytpEnumerator.Fetch()
            End If
        End Sub


        Public Shared Function IsPlaylist(url As String) As Boolean
            If String.IsNullOrEmpty(url) Then Return False
            Return Not String.IsNullOrEmpty(YtPlaylistEnumerator.RxList.MatchGroupValue(url, 2))
        End Function

        Public Shared Function GetListId(url As String) As String
            If Not url.Contains("http://") AndAlso Not url.Contains("https://") Then Return url
            Dim id = YtPlaylistEnumerator.RxList.MatchGroupValue(url, 2)
            If (id IsNot Nothing And id.Contains("#")) Then
                id = id.Substring(0, id.IndexOf("#"))
            End If
            Return id
        End Function

        ''' <summary>
        ''' Add a cached video, in order to work with pre-cached lists.
        ''' </summary>
        ''' <param name="ytvideo"></param>
        Public Sub Add(ytvideo As YtVideo)
            If CachedItems Is Nothing Then CachedItems = New List(Of YtVideo)()
            CachedItems.Add(ytvideo)
        End Sub
        ''' <summary>
        ''' Handle enumerator moves
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="pos"></param>
        ''' <param name="video"></param>
        Private Sub HandleMove(sender As YtPlaylistEnumerator, pos As Int64, video As YtVideo)
            Add(video)
            CacheIndex = pos
        End Sub


        Public Function GetEnumerator() As IEnumerator(Of YtVideo) Implements IEnumerable(Of YtVideo).GetEnumerator
            If CachedItems Is Nothing Then
                Dim ytpEnumerator = New YtPlaylistEnumerator(Url, DownloadOptions)
                AddHandler ytpEnumerator.Moved, AddressOf HandleMove
                AddHandler ytpEnumerator.ResolvedPlaylist, AddressOf HandlePlaylistResolution
                Return ytpEnumerator
            Else
                Return CachedItems.GetEnumerator()
            End If
            'Return New YtPlaylistEnumerator(Url, DownloadOptions)
        End Function

        Public Function GetEnumerator1() As IEnumerator Implements IEnumerable.GetEnumerator
            If CachedItems Is Nothing Then
                Dim ytpEnumerator = New YtPlaylistEnumerator(Url, DownloadOptions)
                AddHandler ytpEnumerator.ResolvedPlaylist, AddressOf HandlePlaylistResolution
                AddHandler ytpEnumerator.Moved, AddressOf HandleMove
                Return ytpEnumerator
            Else
                Return CachedItems.GetEnumerator()
            End If
        End Function

        Public Sub HandlePlaylistResolution(title As String, author As String)
            Me.Name = title
            Me.CreatorName = author
        End Sub
    End Class
End Namespace