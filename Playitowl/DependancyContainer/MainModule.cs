using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject.Modules;
using Playitowl.Data.Repositories;
using Playitowl.Models;
using Playitowl.Service;

namespace Playitowl.DependancyContainer
{
    public class MainModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IVideoService>().To<VideoService>();
            this.Bind<IPlaylistService>().To<PlaylistService>();
            this.Bind<IVideoRepository>().To<VideoRepository>();
            this.Bind<IPlaylistRepository>().To<PlaylistRepository>();
            this.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();

        }
    }
}