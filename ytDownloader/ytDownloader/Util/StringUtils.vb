﻿Imports System.Text.RegularExpressions

Namespace Util

    Public module StringUtils

        Public Function IndexesOfRxEx(src As String, regx As Regex) As List(Of StringIndex)
	        Dim ixs As New List(Of StringIndex)()
	        Dim li As Int32 = 0
	        Dim ix As Int32 = 0
	        Dim indexesof As List(Of String) = Matches(src, regx)
	        If indexesof Is Nothing Then
		        Return Nothing
	        End If
	        For Each indexof As String In indexesof
		        Do
			        If Not (li > src.Length) Then
				        ix = src.ToLower().IndexOf(indexof.ToLower(), li)
			        End If
			        If ix = -1 Then
				        Exit Do
			        End If
			        ' TODO: might not be correct. Was : Exit Do
			        li = ix + indexof.Length
			        ixs.Add(New StringIndex(indexof, ix))
				        ' TODO: might not be correct. Was : Exit Do
			        Exit Do
		        Loop While True
	        Next
	        Return ixs
        End Function

        Public Function Matches(contentToTest As String, ParamArray regexString As String()) As List(Of String)
            If contentToTest Is Nothing Then
                Return Nothing
            End If
            Dim output As New List(Of String)()
            For Each rxString As String In regexString
                Dim currentMatches As MatchCollection = Regex.Matches(contentToTest, rxString, RegexOptions.IgnoreCase)
                If output.Count > 0 Then
                    For i As Int32 = 0 To currentMatches.Count - 1
                        Try
                            output(i) = output(i) + "|$DF%GÛ|" + currentMatches(i).Value
                        Catch
                            output.Add("|$DF%GÛ|" + currentMatches(i).Value)
                        End Try
                    Next
                Else
                    For Each m As Match In currentMatches
                        output.Add(m.Value)
                    Next
                End If
            Next
            Return output
        End Function

        
        Public Function Matches(content As String, regex As Regex) As List(Of String)
	        If content Is Nothing Then
		        Return Nothing
	        End If
	        Dim output As New List(Of String)()
	        Dim mtcs As MatchCollection = regex.Matches(content)
	        If output.Count > 0 Then
		        For i As Int32 = 0 To mtcs.Count - 1
			        Try
				        output(i) = output(i) + "|$DF%GÛ|" + mtcs(i).Value
			        Catch
				        output.Add("|$DF%GÛ|" + mtcs(i).Value)
			        End Try
		        Next
	        Else
		        For Each m As Match In mtcs
			        output.Add(m.Value)
		        Next
	        End If
	        Return output
        End Function

    End module
End NameSpace