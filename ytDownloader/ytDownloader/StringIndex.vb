﻿Public Class StringIndex
    Public property Value As Integer
    Public property Content as String
    Public readonly property Length As Integer
        Get
            If String.IsNullOrEmpty(Content) then 
                return 0
            Else
                Return Content.Length
            End If 
        End Get 
    End Property

    Public Sub New(str As String, loc As Int32)
        Value = loc
        Content = str

    End Sub
End Class