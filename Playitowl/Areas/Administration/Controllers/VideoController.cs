﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Playitowl.Service;

namespace Playitowl.Areas.Administration.Controllers
{
    [Authorize(Roles ="AppAdmin")]
    public class VideoController : Controller
    {
        private IVideoService _videoService;
        private IPlaylistService _playlistService;


        public VideoController(IVideoService videoService, IPlaylistService plService)
        {
            _videoService = videoService;
            _playlistService = plService;
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        { 
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var videos = _videoService.GetSorted(sortOrder);
            if (!String.IsNullOrEmpty(searchString))
            {
                videos = videos.Where(s => s.Name.Contains(searchString));
            } 
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(videos.ToPagedList(pageNumber, pageSize));
             
        } 

        public ActionResult Delete(string id)
        {
            _videoService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}