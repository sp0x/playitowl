Imports System.IO
Imports System.Net

Namespace Extraction
    Public module WebRequestHelper
        Public property AndroidUseragent As String = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
        public Function Fetch(url As String, optional Cookies As CookieContainer = Nothing, Optional userAgent As String = nothing) As String
            If String.IsNullOrEmpty(url) Then Return String.Empty
            If Not url.ToLower.StartsWith("https") Then
                url = url.Replace("http://", "https://")
            End If
             
            dim httpClient As HttpWebRequest = HttpWebRequest.Create(url)
            httpClient.CookieContainer = Cookies
            httpClient.AutomaticDecompression = DecompressionMethods.Deflate Or DecompressionMethods.GZip
            If Not String.IsNullOrEmpty(userAgent) Then
                httpClient.UserAgent = userAgent 
            End If
            HttpClient.Headers(HttpRequestHeader.AcceptEncoding) = "gzip, deflate"
            ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(
                Function()
                    Return True
                End Function)

            Dim response = HttpClient.GetResponse()
            Dim respStrm As Stream = response.GetResponseStream
            Return New StreamReader(respStrm).ReadToEnd()
        End Function
    End module
End NameSpace