﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization; 

using Playitowl.Models;
using Playitowl.Models.Music.Entities;
using Playitowl.Models.Music.Entities.Metadata;
using Playitowl.Models.Repl;

//using nvoid.db.Extensions;
//using nvoid.db.Replex;
//using nvoid.db.Replex.Replex;

namespace playitowl.web.Music
{
    


    internal static class WebRequestHelper
    {
        private static RepLoop Pooler = RepLoop.Factory.Create(RepLoopType.OrderedParallelBlock, 6);


        /// <summary>
        /// T Needs to be the type of the wrapper to use
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="withoutMetadata"></param>
        /// <returns></returns>
        public async static Task<MusicalEntityList<T>> GetAsync<T>(string url, bool withoutMetadata = true)
            where T : MusicalEntity
        {
            Configuration.UserAgent = Configuration.UserAgent;
            var t = typeof (T);
            var tlib = TypeDict.Instance[typeof (T)];
            //
            dynamic metadataWrapper = await GetAsync(tlib[TypeCategory.MetaWrapper], url, withoutMetadata);
            if (metadataWrapper == null) return null;
            IMusicalEntity col = metadataWrapper.Collection;
            return (MusicalEntityList<T>) col;
        }


        /// <summary>
        /// Return
        /// </summary>
        /// <param name="t"></param>
        /// <param name="url"></param>
        /// <param name="withoutMetadata"></param>
        /// <returns>
        /// Returns a metatype type
        /// </returns>
        private static Task<IWrapper> GetAsync(Type t, string url, bool withoutMetadata = true)
        {
            Configuration.UserAgent = Configuration.UserAgent;
            try
            {
                var client = CreateHttpClient(true, Configuration.Proxy);
                IWrapper cache = null;
                var promise = new Task<IWrapper>(() =>
                {
                    return cache;
                });
                Pooler.Push<SpooledAction, IWrapper>((State e) =>
                {
                    try
                    {
                        return DeserializeStream(t, client.GetStreamAsync(url).Result, withoutMetadata);
                    }
                    catch (Exception e2)
                    {
                        e2 = e2;
                        return null;
                    }
                }, (resolved) =>
                {
                    cache = resolved;
                    promise.Start();
                });
                return promise;
            }
            catch (Exception e)
            {
                if (Configuration.GenerateCommunicationThrow)
                {
                    throw e;
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <param name="withoutMetadata"></param>
        /// <returns></returns>
        internal static T DeserializeStream<T>(Stream stream, bool withoutMetadata) where T : MetadataWrapper
        {
            return (T)DeserializeStream(typeof (T), stream, withoutMetadata);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="stream"></param>
        /// <param name="withoutMetadata"></param>
        /// <returns></returns>
        internal static IWrapper DeserializeStream(Type t,Stream stream, bool withoutMetadata)
        {
            var targetType = t;
            if (stream == null)
            {
                throw new NullReferenceException(Resources.Messages.EmptyStream);
            }
            try
            {
                var xml = XDocument.Load(stream);
                var serialize = new XmlSerializer(targetType);
                //Add extension namespace:
                var ns = new XmlSerializerNamespaces();
                ns.Add("ext", "http://musicbrainz.org/ns/ext#-2.0");
                //check valid xml schema:
                if (xml.Root == null || xml.Root.Name.LocalName != "metadata")
                {
                    throw new NullReferenceException(Resources.Messages.WrongResponseFormat);
                }
                var node = withoutMetadata ? xml.Root.Elements().FirstOrDefault() : xml.Root;

                if (node == null)
                {
                    return null;
                }
                var nodeReader = node.CreateReader();
                var res = serialize.Deserialize(nodeReader);
                return (IWrapper)res;
            }
            catch (Exception e)
            { 
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="automaticDecompression"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        private static HttpClient CreateHttpClient(bool automaticDecompression = true, IWebProxy proxy = null)
        {
            var handler = new HttpClientHandler();

            if (proxy != null)
            {
                handler.Proxy = proxy;
                handler.UseProxy = true;
            }

            if (automaticDecompression)
            {
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            var client = new HttpClient(handler);

            client.DefaultRequestHeaders.Add("User-Agent", Configuration.UserAgent);

            return client;
        }
    }
}
