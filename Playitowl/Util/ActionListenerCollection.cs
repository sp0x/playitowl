﻿using System;
using System.Collections.Generic;

namespace Playitowl.Util
{
    public class ActionListenerCollection : Dictionary<Guid, LinkedAction>
    {
        public event EventHandler ActionResponded;
        public Action<Action> Add(LinkedAction value)
        {
            base.Add(value.LinkId, value);
            return (callback) =>
            {
                //Whenever the responce is invoked, issue the callback
                value.Invoked += (sender, e) =>
                {
                    callback();
                };
            };
        }

        public void Raise(String linkedActionId)
        {
            var linkid = new Guid(linkedActionId);
            if (base.ContainsKey(linkid))
            {
                base[linkid].FireInvoke();
            }
            ActionResponded?.Invoke(this, null);
        }
    }
}