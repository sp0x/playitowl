﻿using System;
using System.Xml.Serialization;
using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities.Metadata
{
    [XmlRoot("metadata", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class ArtistMetadata : MetaListWrapper<ArtistList>
    {
        /// <summary>
        /// Gets or sets the artist-list collection.
        /// </summary>
        [XmlElement("artist-list")]
        public override ArtistList Collection { get; set; }
        
        public override string EntityType => "metadata"; 
        public override string Query { get; set; }
        public override Type WrapperType { get; } 
    }
}
