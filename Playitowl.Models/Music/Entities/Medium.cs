﻿using System;
using System.Xml.Serialization;
using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("medium")]
    public class Medium : MusicalEntity
    {
        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        [XmlElement("format")]
        public string Format { get; set; }

        /// <summary>
        /// Gets or sets the disc-list.
        /// </summary>
        [XmlElement("disc-list")]
        public DiskList Disks { get; set; }

        /// <summary>
        /// Gets or sets the track-list.
        /// </summary>
        [XmlElement("track-list")]
        public TrackList Tracks { get; set; }

        public override string EntityType => "medium";
        public override Type WrapperType => null;
        public override string Query { get; set; }
        public override Type ListType => typeof (MediumList);
    }

    [XmlRoot("disk-list")]
    public class DiskList
    {
        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        [XmlAttribute("count")]
        public int Count { get; set; }
    }
}
