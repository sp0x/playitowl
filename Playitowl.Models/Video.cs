﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ytDownloader.Extraction;

namespace Playitowl.Models
{
    public class Video
    { 
        /// <summary>
        /// 
        /// </summary>
        public virtual Playlist Playlist { get; set; }
        [Required]
        public string Id { get; set; }
        public IEnumerable<VideoCodecInfo> Codecs { get; set; }
        public bool Available { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int Duration { get; set; }
        public int ViewCount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Uploaded on")]
        public System.DateTime UploadDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Added on")]
        public System.DateTime Added { get; set; }

        public string Description { get; set; }
        /// <summary>
        /// A song - timestamp list
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> InnerItems { get; set; }

        public string Thumb { get; set; }


        public virtual ICollection<VideoComment> Comments { get; set; }

        public Video()
        { 
        }

        public Video(YtVideo baseVideo)
        {
            Id = baseVideo.Id;
            Codecs = baseVideo.Codecs;
            Available = baseVideo.Available;
            Name = baseVideo.Name;
            Author = baseVideo.Author;
            Duration = (int)baseVideo.Duration;
            ViewCount = (int)baseVideo.ViewCount;
            UploadDate = baseVideo.UploadDate;
            Added = baseVideo.Added;
            Description = baseVideo.Description;
            InnerItems = baseVideo.InnerItems;
            Thumb = baseVideo.Thumb;
        }
        /// <summary>
        /// Gets the cover url for this video
        /// </summary>
        /// <returns></returns>
        public string GetCoverUrl()
        { 
            return YtVideo.GetVideoCoverUrl(Id);
        }
    }
}
