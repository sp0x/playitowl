﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fleck.Helpers;
using Playitowl.Models; 
using Playitowl.Models.Vm;
using Playitowl.Service;

namespace Playitowl.Areas.Main.Controllers
{
    [RoutePrefix("Download")]
    public class DownloadsController : Controller
    {
        private IVideoService _videoService;
        private IPlaylistService _playlistService;
         

        public DownloadsController(IVideoService videoService, IPlaylistService plService)
        {
            _videoService = videoService;
            _playlistService = plService;
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <returns></returns>
        [HttpGet]
        [Route("{resource}")]
        public ActionResult Download(String resource)
        {
            var resourceClean = Path.GetFileNameWithoutExtension(resource);
            var extension = Path.GetExtension(resource).TrimStart('.');
            Video targetVideo = _videoService.GetById(resourceClean);
            if (targetVideo == null)
            {
                var playlist = _playlistService.GetByVideoId(resourceClean);
                if (playlist != null)
                {
                    targetVideo = playlist.Videos?.FirstOrDefault(x => x.Id == resourceClean);
                }
            }
            if (targetVideo == null) return View("Error");
            var name = targetVideo.Name ?? targetVideo.Id;
            var targetFileName = $"{name}.mp3";
            var absPath = Path.Combine(HttpRuntime.AppDomainAppPath, "downloads", $"{resourceClean}.{extension}");
            if (!System.IO.File.Exists(absPath))
            {
                return View("Error");
            }
            else
            {
                var finfo = new FileInfo(absPath);
                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", HttpHelper.GetDisposition(Request, targetFileName));
                Response.AddHeader("Content-Length", finfo.Length.ToString());
                Response.ContentType = "binary/octed-stream";
                Response.Flush();
                Response.TransmitFile(absPath);
                try
                {
                    Response.End();
                }
                catch (Exception e)
                {

                }
            }
            return null;
        }
         
    }
}