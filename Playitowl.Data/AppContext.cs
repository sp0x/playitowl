using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Playitowl.Models;

namespace Playitowl.Data
{
    public class AppContext : IdentityDbContext<ApplicationUser>,
        IAppContext
    {
        public IDbSet<UserDownload> Downloads { get; set; }
        public IDbSet<Video> Videos { get; set; }
        public IDbSet<Playlist> Playlists { get; set; } 
        public IDbSet<VideoComment> VideoComments { get; set; }
        public IDbSet<PlaylistComment> PlaylistComments { get; set; }


        public DbContext DbContext => this;

        // Your context has been configured to use a 'Video' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Playitowl.Models.Video' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Video' 
        // connection string in the application configuration file.
        public AppContext()
            : base("Video")
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static AppContext Create()
        {
            return new AppContext();
        }

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        } 

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    } 
}