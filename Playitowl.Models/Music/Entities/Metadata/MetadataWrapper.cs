﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Metadata
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MetadataWrapper : MusicalEntity, IWrapper
    {
        public abstract IEnumerable getItems();
        public abstract IEnumerable<T> getItems<T>() where T : MusicalEntity;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MetadataWrapper<T> : MetadataWrapper
        where T : IMusicalEntityCollection
    {
        /// <summary>
        /// A list entity
        /// </summary>
        [XmlIgnore] public abstract T Collection { get; set; }
        public override IEnumerable getItems()
        {
            return Collection.getItems();
        }
        public override IEnumerable<TX> getItems<TX>() 
        {
            return Collection.getItems().Cast<TX>();
        }

        public int Add(Object val)
        {
            val = val;
            return 0;
        }
        //        public IEnumerable Items
        //        {
        //            get { return getItems(); }
        //        }
        //        public IEnumerable ItemsEx<T>()
        //        {
        //            return getItems().Cast<T>();
        //        } 

    }
}
