﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Playitowl.Areas.Administration.Controllers
{
    [Authorize(Roles ="AppAdmin")]
    public class Administration : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

    }
}