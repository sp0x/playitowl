using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("life-span", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class LifeSpanNode
    {
        /// <summary>
        /// Gets or sets the begin date.
        /// </summary>
        [XmlElement("begin")]
        public string Begin { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        [XmlElement("end")]
        public string End { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the life-span ended or not.
        /// </summary>
        [XmlElement("ended")]
        public bool Ended { get; set; }
    }
}