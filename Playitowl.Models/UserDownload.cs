﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playitowl.Models
{
    public enum DownloadType
    {
        Video, Playlist
    }
    public class UserDownload
    {
        public int Id { get; set; }
        [Required]
        public DownloadType Type { get; set; }
        public DateTime Created { get; set; }
        public virtual ApplicationUser User { get; set; }


    }
}
