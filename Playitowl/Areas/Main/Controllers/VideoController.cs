﻿using System;
using System.Web.Mvc;
using Playitowl.Models; 
using Playitowl.Models.Vm;
using Playitowl.Service;

namespace Playitowl.Areas.Main.Controllers
{
    public class VideoController : Controller
    {
        private IVideoService _videoService;
        private IPlaylistService _playlistService;
         

        public VideoController(IVideoService videoService, IPlaylistService plService)
        {
            _videoService = videoService;
            _playlistService = plService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetVideoInfoRes(String id, String url = null)
        {
            Video targetVideo = _videoService.Resolve(url ?? id);
            VideoVm model = AutoMapper.Mapper.Map<VideoVm>(targetVideo);
            return PartialView("TargetVideo", model);
        }
         

    }
}