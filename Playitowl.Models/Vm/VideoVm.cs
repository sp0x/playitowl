﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playitowl.Models.Vm
{
    public class VideoVm
    {
        public Boolean IsPlaylist { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string CoverUrl { get; set; }
        [UIHint("Image")]
        public string Thumbnail { get; set; }
    }
}
