﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using Playitowl.Models;
using Playitowl.Service;

namespace Playitowl.Areas.Administration.Controllers
{
    [Authorize(Roles = "AppAdmin")]
    public class UsersController : Controller
    {

        private IVideoService _videoService;
        private IPlaylistService _playlistService;
        private ApplicationUserManager _userManager;

        public UsersController(IVideoService videoService, IPlaylistService plService)
        {
            _videoService = videoService;
            _playlistService = plService; 
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ApplicationUserManager userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            IEnumerable<ApplicationUser> users = null;
            switch (sortOrder)
            {
                case "name_desc":
                    users = userManager.Users.OrderByDescending(s => s.UserName);
                    break;
                case "Date":
                    users = userManager.Users.OrderBy(s => s.RegisteredOn);
                    break;
                case "date_desc":
                    users = userManager.Users.OrderByDescending(s => s.RegisteredOn);
                    break;
                default:
                    users = userManager.Users.OrderBy(s => s.UserName);
                    break;
            }


            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.UserName.Contains(searchString));
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Delete(string id)
        {
            ApplicationUserManager userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
            {
                userManager.Delete(user);
            }
            return RedirectToAction("Index");
        }

    }
}