﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Playitowl
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapMvcAttributeRoutes();
            AreaRegistration.RegisterAllAreas();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //            routes.MapRoute(
            //                name: "Downloads",
            //                url: "Video/{id}",
            //                defaults: new { controller = "Home", action = "Download" }); 
            var playlistRoute = routes.MapRoute(
                name: "Playlist",
                url: "Playlist/v/{name}/{id}",
                defaults: new { controller = "Playlist", action = "ViewPlaylist" },
                namespaces: new[] { "Playitowl.Areas.Main.Controllers" }
            );
            playlistRoute.DataTokens["area"] = "Main";

            var playlistDownloadRoute = routes.MapRoute(
                name: "PlaylistDownload",
                url: "Playlist/Download/{id}",
                defaults: new { controller = "Playlist", action = "Download" },
                namespaces: new[] { "Playitowl.Areas.Main.Controllers" }
            );
            playlistDownloadRoute.DataTokens["area"] = "Main";

            //routes.MapRoute(name: "Root", url: "", defaults: new {controller = "Home", action = "Index", area = "main" });
            var adminRoute = routes.MapRoute(
                "Admin_default",
                "admin/",
                new { controller = "Administration", action = "Index" },
                new[] { "Playitowl.Areas.Administration.Controllers" }
               );
            adminRoute.DataTokens["area"] = "Administration"; 
            


            //            routes.MapRoute(
            //                name: "AreaSeparatedRoute",
            //                url: "{area}/{controller}/{action}/{id}",
            //                defaults: new { area = "Main", controller = "Home", action="Index", id = UrlParameter.Optional},
            //                namespaces: new[] {
            //                    "Playitowl.Controllers",
            //                    "Playitowl.Areas.Administration.Controllers",
            //                    "Playitowl.Areas.Main.Controllers"
            //                }
            //            );
            //            routes.MapRoute(
            //                name: "Default",
            //                url: "{controller}/{action}/{id}",
            //                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //            );
        }
    }
}
