﻿using System;
using System.Threading.Tasks;
using playitowl.web.Music;

namespace Playitowl.Models.Music.Entities
{
    /// <summary>
    /// Base class for any entity returned by the MusicBrainz XML webservice.
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Sends a lookup request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to lookup.</param>
        /// <param name="id">The MusicBrainz id of the entity.</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntity> LookupAsync<T>(string entity, string id, params string[] inc) 
            where T : IMusicalEntityCollection
        {
            if (string.IsNullOrEmpty(entity))
            {
                throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "id"));
            }

            var rq = MusicalEntity.CreateUrl(entity, id, MusicalEntity.RequestType.Lookup, null, null, String.Join("+", inc));
            return null;
            ///return (MusicalEntity)(await WebRequestHelper.GetAsync<T>(rq))..FirstOrDefault();
        }

        /// <summary>
        /// Sends a search request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to search for.</param>
        /// <param name="query">The query string.</param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, default = 0).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntityList<T>> SearchAsync<T>(MusicalEntity entity, int limit = 25, int offset = 0) 
            where T : MusicalEntity
        {
            if (entity == null) throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            var searchRequest = entity.CreateUrl(MusicalEntity.RequestType.Search,null,null,null,limit, offset);
            return await WebRequestHelper.GetAsync<T>(searchRequest, withoutMetadata: false);
        }
        
        /// <summary>
        /// Sends a browse request to the webservice.
        /// </summary>
        /// <typeparam name="T">Any type derived from <see cref="Entity"/>.</typeparam>
        /// <param name="entity">The name of the XML entity to browse.</param>
        /// <param name="relatedEntity"></param>
        /// <param name="relatedEntityId"></param>
        /// <param name="limit">The number of items to return (default = 25).</param>
        /// <param name="offset">The offset to the items list (enables paging, de fault = 0).</param>
        /// <param name="inc">A list of entities to include (subqueries).</param>
        /// <returns></returns>
        protected async static Task<MusicalEntityList<T>> BrowseAsync<T>(MusicalEntity entity, string relatedEntity, string relatedEntityId, int limit, int offset, params  string[] inc) 
            where T : MusicalEntity
        {
            if (entity == null)
            {
                throw new ArgumentException(string.Format(playitowl.web.Music.Resources.Messages.MissingParameter, "entity"));
            }
            var searchRequest = entity.CreateUrl(MusicalEntity.RequestType.Browse, relatedEntityId, relatedEntity, String.Join("+", inc), limit, offset);
            return await WebRequestHelper.GetAsync<T>(searchRequest, withoutMetadata: false);
        }
    }
}
