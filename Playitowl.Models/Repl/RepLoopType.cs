namespace Playitowl.Models.Repl
{
    public enum RepLoopType
    {
        /// <summary>
        /// Triggers would be fired in an orderly fashion, one after another, with blocking.
        /// </summary>
        OrderedBlock,
        /// <summary>
        /// Triggers are fired without blocking one after another, in parallel.
        /// </summary>
        Async,
        /// <summary>
        /// NOT IMPLEMENTED!
        /// Triggers are triggered only by events, instead of the standart polling thread.
        /// </summary>
        Evented,
        /// <summary>
        /// Similar to the Ordered block, but multiple tasks run in parallel, in a single block
        /// </summary>
        OrderedParallelBlock,
    }
}