namespace Playitowl.Models
{
    public class VideoComment : Comment
    { 
        public virtual Video Video { get; set; }
    }
}