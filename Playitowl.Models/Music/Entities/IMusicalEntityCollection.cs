﻿using System;
using System.Collections;

namespace Playitowl.Models.Music.Entities
{
    public interface IMusicalEntityCollection
    {
        IEnumerable getItems();
        Object FirstOrDefault(); 
    }
}