﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Playitowl.Models.Music.Entities;

namespace playitowl.web.Music
{
    public class MusicBrainz
    {
         
        private static async Task<Artist> GetArtist(string name)
        {
            var artists = await new Artist(name).GetAll();
            var artist = artists.Cast<Artist>().First();
            Console.WriteLine(artist.Name);
            return artist;
        }

        public static async Task<Track> GetTrack(string val)
        {
            IEnumerable<Track> x = (await new Track(val).GetAll()).Cast<Track>();
            return x.FirstOrDefault();
        }

        private static async void ShowTracksByAlbum(string artistName, string albumName)
        {
            Configuration.UserAgent = "Chrome/41.0.2228.0";
            var artist = await GetArtist(artistName);
            var release = new Release();
            var albums = await release.GetRelated("artist", artist.Id);
            var album = albums.Items.FirstOrDefault(r => r.Title.ToLower() == albumName.ToLower());
            if (album == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Album not found.\n");
                Console.ResetColor();

                Console.WriteLine("Results:\n");
                foreach (var al in albums.Items)
                {
                    Console.WriteLine("\t{0}", al.Title);
                }

                return;
            }

            Console.WriteLine("\t{0}", albumName);
            var rec = new Recording();
            var tracks = await rec.BrowseAsync("release", album.Id, 100);
            foreach (var track in tracks.Items)
            {
                Console.WriteLine("\t\t{0}", track.Title);
            }
        }

        private static async void ShowAlbumsTracksByArtist(string name)
        {
            var artist = await GetArtist(name);
            var rel = new Release();
            var rec = new Recording();
            var releases = await rel.GetRelated("artist", artist.Id, 100, 0, "media");

            foreach (var release in releases.Items)
            {
                Console.WriteLine("\t{0}", release.Title);
                var tracks = await rec.BrowseAsync("release", release.Id, 100);
                foreach (var track in tracks.Items)
                {
                    Console.WriteLine("\t\t{0}", track.Title);
                }
            }
        }


        private static async void Test()
        {
            var artists = await new Artist("The Scorpions").GetAll();
            Artist artist = artists.Cast<Artist>().FirstOrDefault();
            Console.WriteLine(artist.Name);
            var recordings = await new Recording().BrowseAsync("artist", artist.Id, 40);
            foreach (var recording in recordings.Items)
            {
                Console.WriteLine("{0}", recording.Title);
            }
        }
    }
}