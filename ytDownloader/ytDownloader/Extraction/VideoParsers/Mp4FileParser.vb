Imports ytDownloader.Extraction.AudioExtractors

Namespace Extraction.VideoParsers
    Friend Class Mp4FileParser
        Implements IChannelExtractor
        Private _input As String
        Private _output As String
        
        Public Event ConversionProgressChanged As EventHandler(Of ProgressEventArgs)

#Region "Construction"
        Public Sub New(input As String, output As String)
            _input = input
            _output = output
        End Sub
#End Region

        Public Sub ExtractStreams() 
            Dim pathToFfmpeg As String = FfmpegHelper.GetPath()
            
            Dim stInfo = New ProcessStartInfo()
            stInfo.UseShellExecute = False
            stInfo.RedirectStandardError = True
            stInfo.FileName = pathToFfmpeg
            dim ffmpeg = New Process() With { .StartInfo = stInfo}

            'Dim arguments = [String].Format("-i ""{0}"" -c:a flac ""{1}""", _input, _output)
            Dim arguments = [String].Format("-i ""{0}"" ""{1}"" -y", _input, _output)
            ffmpeg.StartInfo.Arguments = arguments

            Try
                If Not ffmpeg.Start() Then
                    Debug.WriteLine("Error starting")
                    Return
                End If
                Dim reader = ffmpeg.StandardError
                Dim line As String
                While (InlineAssignHelper(line, reader.ReadLine())) IsNot Nothing
                    Debug.WriteLine(line)
                End While
                Dim progress = New ProgressEventArgs(100.0)
                raiseevent ConversionProgressChanged(Me, progress)
            Catch exception As Exception
                Debug.WriteLine(exception.ToString())
                Return
            End Try

            ffmpeg.Close() 
        End Sub
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
            target = value
            Return value
        End Function
        Public Sub Dispose() Implements IDisposable.Dispose
            
        End Sub

        Public Sub IChannelExtractor_ExtractStreams() Implements IChannelExtractor.ExtractStreams
            Throw New NotImplementedException
        End Sub

        Public Function GetAudioExtractor(param As UInteger) As AudioChannelExtractor Implements IChannelExtractor.GetAudioExtractor
            Throw New NotImplementedException
        End Function

        Public Sub CloseOutput(disposing As Boolean) Implements IChannelExtractor.CloseOutput 
        End Sub

    End Class
End NameSpace