﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization; 

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("recording-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class RecordingList : MusicalEntityList<Recording>
    {
        /// <summary>
        /// Gets or sets the list of recordings.
        /// </summary>
        [XmlElement("recording")]
        public override List<Recording> Items { get; set; }

        public override string EntityType => "recording-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
