﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("tag-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class TagList : MusicalEntityList<Tag>
    {
        /// <summary>
        /// Gets or sets the list of tags.
        /// </summary>
        [XmlElement("tag")]
        public override List<Tag> Items { get; set; }

        public override string EntityType => "tag-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType { get; }

        /// <summary>
        /// Gets the most referenced tag. Returns null on failure
        /// </summary>
        /// <returns></returns>
        public Tag GetMostReferencedTag(List<String> filter = null)
        {
            if(filter!=null) return this.Items?.Where(x => filter.Contains(x.Name.ToLower())).OrderBy(x=>x.Count).FirstOrDefault();
            else return this.Items?.OrderBy(x => x.Count).FirstOrDefault();
        }
    }
}
