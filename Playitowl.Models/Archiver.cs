﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ytDownloader;

namespace Playitowl.Models
{
    /// <summary>
    /// Manages the creation of zip files.
    /// </summary>
    public class Archiver
    {
        private List<string> mFiles;
        public List<string> Files => mFiles;
        private string BasePath { get; set; }
        public Archiver(string[] files)
        {
            Uri partUriDocument = null;
            mFiles = new List<string>(files);
            //            foreach (string file in files)
            //            {
            //                //partUriDocument = PackUriHelper.CreatePartUri(new Uri(file, UriKind.Relative));
            //                mFiles.Add(partUriDocument);
            //            }
        }

        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private static extern System.UInt32 FindMimeFromData(
            System.UInt32 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt32 cbSize,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt32 dwMimeFlags,
            out System.UInt32 ppwzMimeOut,
            System.UInt32 dwReserverd
        );

        private static string getMimeFromFile(string filename)
        {
            if (!File.Exists(filename)) throw new FileNotFoundException(filename + " not found");

            byte[] buffer = new byte[256];
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                if (fs.Length >= 256)
                    fs.Read(buffer, 0, 256);
                else
                    fs.Read(buffer, 0, (int)fs.Length);
            }
            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch (Exception e)
            {
                return "unknown/unknown";
            }
        }

        public static string ResolveMime(string file)
        {
            var ext = System.IO.Path.GetExtension(file)?.ToLower();
            if (string.IsNullOrEmpty(ext) && !string.IsNullOrEmpty(file)) return getMimeFromFile(file);
            if (ext.StartsWith(".")) ext = ext.Substring(1);
            if (ext == "xml") return System.Net.Mime.MediaTypeNames.Text.Xml;
            else if (ext == "mp4") return "video/mp4";
            else if (ext == "mkv") return "video/x-matroska";
            else if (ext == "flv") return "video/x-flv";
            else if (ext == "3gp") return "video/3gpp";
            else if (ext == "avi") return "video/x-msvideo";
            else if (ext == "mp3") return "audio/mpeg3";
            else return getMimeFromFile(file);
        }

        //  -------------------------- CreatePackage -------------------------- 
        /// <summary> 
        ///   Creates a package zip file containing specified 
        ///   content and resource files.</summary> 
        public void CreatePackage(string packageDestination, Func<string, string> filenameAggregate = null)
        {
            using (Package package = Package.Open(packageDestination, FileMode.Create))
            {
                foreach (string flToPack in Files)
                {
                    var path = flToPack;
                    var packageItemPath = flToPack;

                    if (!string.IsNullOrEmpty(BasePath))
                    {
                        packageItemPath = path.Substring(BasePath.Length);
                        packageItemPath = packageItemPath.TrimStart('\\');
                    }
                    if (filenameAggregate != null)
                    {
                        var fileName = Path.GetFileName(packageItemPath);
                        var newFileName = filenameAggregate(fileName);
                        newFileName = newFileName.RemoveIllegalPathCharacters();
                        if (string.IsNullOrEmpty(newFileName)) newFileName = fileName;
                        if (0 == (packageItemPath.Length - fileName.Length))
                        {
                            packageItemPath = newFileName.RemoveIllegalPathCharacters();
                        }
                        else
                        {
                            packageItemPath = packageItemPath.Substring(packageItemPath.Length - fileName.Length) +
                                              newFileName.RemoveIllegalPathCharacters();
                        }
                    }
                    packageItemPath = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.UrlDecode(packageItemPath));
                    var itemUri = PackUriHelper.CreatePartUri(new Uri(packageItemPath, UriKind.Relative));
                    if (package.PartExists(itemUri)) continue;
                    PackagePart packagePartDocument = package.CreatePart(itemUri, ResolveMime(path));
                    //Copy the data
                    using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        CopyStream(fileStream, packagePartDocument.GetStream());
                    }
                }
            }
        }

        private static void UnzipFilesFromStream(Stream source, string baseFolder)
        {
            if (!Directory.Exists(baseFolder))
            {
                Directory.CreateDirectory(baseFolder);
            }

            using (Package package = Package.Open(source, FileMode.Open))
            {
                foreach (PackagePart zipPart in package.GetParts())
                {
                    // fix for white spaces in file names (by ErrCode)
                    string path = Path.Combine(baseFolder,
                         Uri.UnescapeDataString(zipPart.Uri.ToString()).Substring(1));

                    using (Stream zipStream = zipPart.GetStream())
                    {
                        using (FileStream fileStream = new FileStream(path, FileMode.Create))
                        {
                            zipStream.CopyTo(fileStream);
                        }
                    }
                }
            }
        }


        //  --------------------------- CopyStream --------------------------- 
        /// <summary> 
        ///   Copies data from a source stream to a target stream.</summary> 
        /// <param name="source">
        ///   The source stream to copy from.</param> 
        /// <param name="target">
        ///   The destination stream to copy to.</param> 
        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }// end:CopyStream()

        public void setBasePath(string basePath)
        {
            this.BasePath = basePath;
        }
    }
}
