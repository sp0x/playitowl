﻿using System.Collections.Generic;
using Playitowl.Models;

namespace Playitowl.Service
{
    public interface IVideoService
    { 
        IEnumerable<Video> ListVideos();
        Video Resolve(string s);
        Video GetById(string id);
        void SaveVideo(Video yvideo);
        void Remove(string id);
        IEnumerable<Video> GetSorted(string sortOrder);
    }
}