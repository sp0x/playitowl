using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.ModelBinding;
using Playitowl.Data.Repositories;
using Playitowl.Models;
using ytDownloader.Extraction;

namespace Playitowl.Service
{
    public class PlaylistService : IPlaylistService
    {
        private ModelStateDictionary _modelState;
        private IPlaylistRepository _playlistRepo;
        private IVideoRepository _videoRepo;

        public PlaylistService(IPlaylistRepository listRepo, IVideoRepository videoRepo)
        {
            _playlistRepo = listRepo;
            _videoRepo = videoRepo;
        }
        public PlaylistService(ModelStateDictionary modelState, IPlaylistRepository listRepo, IVideoRepository videoRepo) : this(listRepo, videoRepo)
        {
            _modelState = modelState;
        }

        protected bool ValidatePlaylist(Playlist productToValidate)
        {
            if (_modelState != null)
            {
                if (productToValidate.Name.Trim().Length == 0)
                    _modelState.AddModelError("Name", "Name is required."); 
                //            if (productToValidate.UnitsInStock < 0)
                //                _modelState.AddError("UnitsInStock", "Units in stock cannot be less than zero.");
                return _modelState.IsValid;
            }
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">See PlaylistType in order to figure out types</param>
        /// <param name="count"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IEnumerable<Playlist> FetchByType(PlaylistType type, int count, int offset)
        { 
            IEnumerable<Playlist> results = null;
            switch (type)
            {
//                case PlaylistType.Popular:
//                    results = _videoRepository.Select(x => x).OrderBy(x => x.Hits).Skip(offset).Take(count);
//                    break;
//                case PlaylistType.Genres:
//                    results = _videoRepository.Select(x => x).OrderBy(x => x.Hits).Skip(offset).Take(count);
//                    break;
//                case PlaylistType.Explore:
//                    results = _videoRepository.Select(x => x).OrderBy(x => x.Hits).Skip(offset).Take(count);
//                    break;
                default:
                    
                    break;
            }
            var orderedQueryable = _playlistRepo.AsQueryable().OrderBy(x => x.Hits);
            return orderedQueryable.Skip(offset).Take(count);
            
        }




        //        public bool Create(YtVideo list)
        //        {
        //            // Validation logic
        //            if (!ValidatePlaylist(productToCreate))
        //                return false;//
        //            // Database logic
        //            try
        //            {
        //                _videoRepository.CreateVideo(productToCreate);
        //            }
        //            catch
        //            {
        //                return false;
        //            }
        //            return true;
        //        }

        public void Create(Playlist list)
        {
            var existingList = _playlistRepo.FindById(list.Id);
            if(existingList==null) _playlistRepo.Create(list);
        }

        public IEnumerable<Playlist> ListVideos()
        {
            throw new NotImplementedException();
        }
         

        public Playlist GetById(string id)
        {
            return _playlistRepo.AsQueryable().FirstOrDefault(x=>x.Id == id);
        }

        public IEnumerable<Playlist> GetAllByPartialName(string searchTerm)
        {
            return _playlistRepo.AsQueryable().Where(x => x.Name.ToLower().Contains(searchTerm));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Playlist GetByVideoId(string id)
        {
            Video video = _videoRepo.GetById(id);
            return video?.Playlist;
        }

        public void SavePlaylist(Playlist model)
        {
            Playlist existingPlaylist = _playlistRepo.FindById(model.Id);
            if (existingPlaylist == null)
            {
                _playlistRepo.Create(model); 
            }
//            if (model.Videos != null)
//            {
//                foreach (Video video in model.Videos)
//                {
//                    _videoRepo.Create(video);
//                }
//            }
        }

        public void SetPlaylistVideos(Playlist model, ICollection<Video> videos)
        {
            _playlistRepo.SetPlaylistVideos(model, videos);
        }

        public IEnumerable<Playlist> GetSorted(string sortOrder)
        {
            IEnumerable<Playlist> playlists = null;
            switch (sortOrder)
            {
                case "name_desc":
                    playlists = _playlistRepo.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    playlists = _playlistRepo.OrderBy(s => s.CreatedOn);
                    break;
                case "date_desc":
                    playlists = _playlistRepo.OrderByDescending(s => s.CreatedOn);
                    break;
                default:
                    playlists = _playlistRepo.OrderBy(s => s.Name);
                    break;
            }
            return playlists;
        }

        public void Remove(string id)
        {
            _playlistRepo.Remove(id);
        }
    }
}