﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playitowl.Models.Extensions
{
    public static class ArrayExtensions
    {
        public static int IndexOfAny(this string src, out string delimiter, params String[] values)
        {
            var delimiters = new Stack<String>();
            delimiter = null;
            for (int i = 0; i < values.Length; i++)
            {
                delimiters.Push(values[i]);
            }
            var index = -1;
            while (delimiters.Count > 0)
            {
                var del = delimiters.Pop();
                if (0 <= (index = src.IndexOf(del)))
                {
                    delimiter = del;
                    break;
                }
            };
            if (index == -1) delimiter = null;
            return index;
        }

    }
}
