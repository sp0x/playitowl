﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playitowl.Models.Vm
{
    public class PlaylistSearchVm
    {
        public PlaylistSearchVm(IEnumerable<Playlist> items)
        {
            this.Items = items.Select(x => AutoMapper.Mapper.Map<PlaylistVm>(x));
        }

        public IEnumerable<PlaylistVm> Items { get; set; }
    }
}
