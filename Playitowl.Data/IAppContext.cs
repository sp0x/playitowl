using System.Data.Entity;
using Playitowl.Models;

namespace Playitowl.Data
{
    public interface IAppContext
    {
        IDbSet<Video> Videos { get; set; } 
        DbContext DbContext { get; }
        int SaveChanges();
        IDbSet<T> Set<T>()
            where T : class;
    }
}