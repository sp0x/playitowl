Imports System.IO
Imports System.Reflection

Namespace Extraction.VideoParsers
    Public Class FfmpegHelper
        Public shared function GetPath() As String
            Dim asm = System.Reflection.Assembly.GetExecutingAssembly()
            Dim directory = AssemblyDirectory()
            Dim ffmpegLocation = Path.Combine(directory, "ffmpeg.exe")
            If Not File.Exists(ffmpegLocation) Then
                CreateFFMpeg(ffmpegLocation)
            End If
            Return ffmpegLocation
        End function

        Private shared sub CreateFFMpeg(path As String)
            File.WriteAllBytes(path, My.Resources.ffmpeg) 
        End sub


        Public Shared ReadOnly Property AssemblyDirectory() As String
	        Get
		        Dim codeBase As String = Assembly.GetExecutingAssembly().CodeBase
		        Dim uri__1 As New UriBuilder(codeBase)
		        Dim path__2 As String = Uri.UnescapeDataString(uri__1.Path)
		        Return Path.GetDirectoryName(path__2)
	        End Get
        End Property
    End Class
End NameSpace