using System;

namespace Playitowl.Util
{
    public class LinkedAction
    {
        public Guid LinkId { get; private set; } = Guid.NewGuid();
        public String action { get; set; }
        public String Id { get; set; }
        public event EventHandler Invoked;

        public void FireInvoke()
        {
            Invoked?.Invoke(this, null);
        }

        public LinkedAction(String name)
        {
            this.action = name;
        }
    }
}