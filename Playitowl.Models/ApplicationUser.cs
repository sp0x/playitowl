﻿using System;
using System.Collections.Generic; 
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework; 

namespace Playitowl.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
         
        public DateTime RegisteredOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<Playlist> Playlists { get; set; }

        public virtual ICollection<Video> Videos { get; set; }
        public virtual ICollection<UserDownload> Downloads { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var applicationCookie = DefaultAuthenticationTypes.ApplicationCookie;
            var userIdentity = await manager.CreateIdentityAsync(this, applicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}