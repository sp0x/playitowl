using System;
using System.Threading.Tasks;

namespace Playitowl.Models.Music.Entities
{
    public class TaskWrapper
    {
        private dynamic Task { get; set; }
        private Type TargetType { get; set; }
        public Type FullType { get; private set; }

        public TaskWrapper(Type oftype, Task t)
        {
            this.TargetType = oftype;
            this.FullType = typeof(Task<>).MakeGenericType(oftype);
            this.Task = t;
            //this.Task = Convert.ChangeType(t, FullType);
            this.Task.ContinueWith(new Action<Object>((o) =>
            {
                o = o;
            }));
        }

        private void onFinished(Object result)
        {
            Convert.ChangeType(Task, typeof(Task<>).MakeGenericType(FullType));
        }
    }
}