﻿using System;
using System.Xml.Serialization;

using Playitowl.Models.Music.Entities.Collections;

namespace Playitowl.Models.Music.Entities
{
    [XmlRoot("track", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class Track : MusicalEntity
    {
        public override string EntityType => "track";
        public override Type WrapperType => null;
        public sealed override string Query { get; set; }

        public Track()
        {
        }

        public Track(String query)
        {
            this.Query = query;
        }

        public override Type ListType => typeof (TrackList);

        /// <summary>
        /// Gets or sets the MusicBrainz id.
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        [XmlElement("position")]
        public int Position { get; set; }

        // <number> is almost always same as <position>, so leaving it

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        [XmlElement("length")]
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets the recording.
        /// </summary>
        [XmlElement("recording")]
        public Recording Recording { get; set; }

        public static Track FromTitle(string title, out Artist artist)
        {
            var track = new Track();
            if (title.IndexOf(" - ") >= 0)
            {
                var parts = title.Split(new string[] { " - " }, StringSplitOptions.None);
                if (parts.Length > 0)
                {
                    artist = new Artist(parts[0]);
                    if (parts.Length > 1)
                    {
                        track.Query = parts[1];
                        track.Query = track.Query.Substring(0, track.Query.IndexOfAny(new char[] {'(', '|'}));
                    }
                }
                else artist = null;


            }
            else artist = null;
            return track;
        }
    }
}
