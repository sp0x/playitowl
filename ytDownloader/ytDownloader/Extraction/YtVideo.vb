﻿Imports System.Globalization
Imports System.Text.RegularExpressions
Imports CsQuery
Imports HtmlAgilityPack
Imports Fizzler
Imports Fizzler.Systems.HtmlAgilityPack
Imports nvoid.db.DB
Imports nvoid.db.Extensions
Imports ytDownloader.Util

Namespace Extraction

    Public Class YtVideo
        Inherits Entity
        Public property Id As String 
        Public Property Codecs As IEnumerable(Of VideoCodecInfo)
        Public Property Available As Boolean
        Public Property Name As String
        Public Property Author As String
        Public Property Duration As  Integer
        Public Property ViewCount As Integer
        Public Property UploadDate As Date
        Public Property Added As Date

        Public Property Description As String
        ''' <summary>
        ''' A song - timestamp list
        ''' </summary>
        ''' <returns></returns>
        Public Property InnerItems As New Dictionary(Of String, String)

        Public Property Thumb As String

        Public Shared ReadOnly RxVideoId As New Regex("(watch\?v=)(.*?)(&|$)", RegexOptions.Compiled Or RegexOptions.IgnoreCase)
        Private Shared ReadOnly RxDate As New Regex("\w{1,2}\.\w{1,2}\.\w{4}", RegexOptions.Compiled Or RegexOptions.IgnoreCase)
        Public Sub New(videoIdOrUrl As String, Optional bAvailable As Boolean = True)
            Me.New(videoIdOrUrl, bAvailable, "")
        End Sub
        Public Sub New()
        End Sub
        Public Sub New(video As YtVideo)
            With video
                Id = .Id
                Available = .Available
                Codecs = .Codecs
                Author = .Author
                Added = .Added
                Description = .Description
                Duration = .Duration
                ViewCount = .ViewCount
                UploadDate = .UploadDate
                Thumb = .Thumb
                Name = .Name
                InnerItems = .InnerItems
            End With
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="videoIdOrUrl">The videoIdOrUrl to use, this can be a link to the video also.</param>
        ''' <param name="bAvailable"></param>
        ''' <param name="pageSource">Parse a videopage in order to fetch information from it</param>
        Public Sub New(videoIdOrUrl As String, bAvailable As Boolean, pageSource As String)
            Id = GetVideoId(videoIdOrUrl)
            Available = bAvailable
            If Not String.IsNullOrEmpty(pageSource) Then
                Dim htmlDoc  As CQ = pageSource

                Dim descObj = htmlDoc("#watch-description-text p#eow-description")
                Dim titleObj = htmlDoc("#eow-title")
                Dim vc = htmlDoc(".watch-view-count")
                Dim uploadedOn = htmlDoc(".watch-time-text")
                Dim tmpStr = String.Empty
                Dim titleObjects = titleObj.ToList()
                If titleObjects.Count > 0 Then
                    Dim titleNode As IDomObject = titleObj.FirstOrDefault()
                    If titleNode IsNot Nothing
                        Name = titleNode.InnerText.ToString().Trim().TrimStart(vbLf).TrimEnd(vbLf)
                    End If
                End If
                If vc.Count > 0 Then
                    Dim views As IDomObject = vc.FirstOrDefault
                    If views IsNot Nothing
                        tmpStr = views.InnerText().Replace(" ", "").Replace(" ", "")
                        tmpStr = tmpStr.Replace("показвания", "").Replace("views", "")
                        Me.ViewCount = ULong.Parse(tmpStr)
                    End If
                End If
                If uploadedOn.Count > 0 Then
                    Dim upNode As IDomObject = uploadedOn.FirstOrDefault()
                    If upNode IsNot Nothing Then
                        tmpStr = StringUtils.Matches(upNode.InnerText, RxDate).FirstOrDefault()
                        UploadDate = Date.ParseExact(tmpStr, "d.MM.yyyy", CultureInfo.InvariantCulture)
                    End If
                End If
                'Parse description
                If descObj.Count > 0 Then
                    Dim descNode As IDomObject = descObj.FirstOrDefault()
                    If descNode IsNot Nothing Then
                        ParseDescriptionRows(descNode.InnerHtml.ToString())
                        If descNode.ChildNodes.Count > 0 Then
                            descNode = descNode.ChildNodes.Item(0)
                            Description = descNode.NodeValue
                        End If
                    End If
                End If
            End If
        End Sub
        ''' <summary>
        ''' Parse the video`s description, in order to find time tables
        ''' </summary>
        ''' <param name="descNode"></param>
        ''' <returns></returns>
        Private Function ParseDescriptionRows(descNode As String)
            Dim descRows = descNode.Split(New String() {"<br>"}, StringSplitOptions.None)
            Dim fn As String = "onclick=""yt.www.watch.player.seekTo"
            Dim pattern = New Regex("(.*)\s*(<a.*?>)(.*?)(</a>)(.*)", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
            Dim indexPattern = New Regex("\d{1,3}\.\s*", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
            Dim validateTitle = _
                Function(row As String, ByRef time As String) As String
                    Dim matches As MatchCollection = pattern.Matches(row)
                    If matches.Count = 0 Then Return Nothing
                    Dim m = matches(0)
                    Dim title As String = m.Groups(1).Value

                    Try
                        If String.IsNullOrEmpty(title) _
                           OrElse title.Length <= 3 _
                           OrElse title.Equals("[") Then title = m.Groups(5).Value
                    Catch ex As Exception

                    End Try
                    title = title.TrimStart("[", "]", " ").TrimEnd(" ", "[", "]")
                    title = indexPattern.Replace(title, "")
                    time = m.Groups(3).Value
                    Return title
                End Function

            For Each descRow In descRows
                If String.IsNullOrEmpty(descRow) Or Not descRow.Contains(fn) Then Continue For
                Dim time As String = String.Empty
                Dim title = validateTitle(descRow, time)
                If (title.Contains("<a href=")) Then
                    title = validateTitle(title, time)
                End If
                Try
                    title = ValidateElementName(title)
                    If (Not InnerItems.ContainsKey(title)) Then
                        InnerItems.Add(title, time)
                    End If
                Catch ex As Exception
                End Try
            Next
            Return True
        End Function

        Public Shared Function ValidateElementName(name As String) As String
            name = name.Replace("(", " as ").Replace(")", "").Replace(" ", "_").Replace("-", "_").Replace("&", "_and_").Replace(".", "_")
            Return name
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        Public Function GetThumbnail() As String
            Dim result = If(Not String.IsNullOrEmpty(Thumb), Thumb, String.Format("//i.ytimg.com/vi/{0}/mqdefault.jpg", Id))
            If Not result.EndsWith("/mqdefault.jpg") And result.Length > 0 Then
                result = result.Replace("/default.jpg", "/mqdefault.jpg")
            End If
            Return result
        End Function

        Public Function GetCodecs() As IEnumerable(Of VideoCodecInfo)
            Dim tmpVideo As YtVideo = Downloader.Factory(Of AudioDownloader).FetchVideo(ToString())
            If tmpVideo Is Nothing Then Return Nothing
            Codecs = tmpVideo.Codecs
            Dim codec = Codecs.FirstOrDefault()
            If tmpVideo IsNot Nothing
                With tmpVideo
                    If (String.IsNullOrEmpty(Name)) Then Name = .Name
                    If (String.IsNullOrEmpty(Description)) Then Description = .Description
                    ViewCount = .ViewCount
                    If (String.IsNullOrEmpty(Author)) Then Author = .Author
                    Added = .Added
                End With
            End If
            Return Codecs
        End Function

        Public Function GetDownloader(options As DownloadOptions, Optional isPlaylist As Boolean = False, Optional lazy As Boolean = True) As Downloader
            Dim result As Downloader = Nothing
            If lazy Then
                result = Downloader.CreateEmpty(ToString(), options, isPlaylist)
            Else
                result = Downloader.Factory.Create(Me, options, isPlaylist, lazy)
            End If
            result.InputUrl = ToString()
            Return result
        End Function

        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Id) Then
                Throw New InvalidOperationException("Video idOrUrl Is null")
            End If
            Return String.Format("https://www.youtube.com/watch?v={0}", Id)
        End Function

        Public Shared Function ToUrl(idOrUrl As String) As String
            If idOrUrl.StartsWith("http://") Or idOrUrl.StartsWith("https://") Then Return idOrUrl
            Return String.Format("https://www.youtube.com/watch?v={0}", idOrUrl)
        End Function

        Public Shared Function GetVideoId(videoId As String) As String
            If String.IsNullOrEmpty(videoId) Then Return videoId
            If Not videoId.Contains("http://") And Not videoId.Contains("https://") Then Return videoId
            Return RxVideoId.MatchGroupValue(videoId, 2)
        End Function
        Public Shared Function GetVideoCoverUrl(videoUrlOrId As String) As String
            Dim id As String = GetVideoId(videoUrlOrId)
            If String.IsNullOrEmpty(id) Then Throw New InvalidOperationException("Invalid Youtube URL!")
            Return String.Format("http://img.youtube.com/vi/{0}/2.jpg", id)
        End Function

        Public Function GetInfoUrl() As String
            return String.Format("http://youtube.com/get_video_info?video_id={0}", Id)
        End Function

        Public Function GetUri() As String
            Return String.Format("https://www.youtube.com/watch?v={0}", Id)
        End Function
    End Class
End Namespace