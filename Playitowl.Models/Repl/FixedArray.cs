using System;

namespace Playitowl.Models.Repl
{
    public class FixedArray<T>
    {
        public int Size { get; set; }

        public int Allocated
        {
            get; private set;
        }

        public bool IsFull => Allocated == Size;
        /// <summary>
        /// 
        /// </summary>
        public T[] Items;

        public FixedArray(int blockThreadSize)
        {
            Size = blockThreadSize;
            Items = new T[Size];
        }

        public void Add(T value)
        {
            if (Allocated == Size) throw new InvalidOperationException();
            Items[Allocated++] = value;
        }
    }
}