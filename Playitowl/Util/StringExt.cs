﻿using System;

namespace Playitowl.Util
{
    public static class StringExt
    {
        public static String UrlDecode(this String url)
        { 
            return System.Web.HttpUtility.UrlDecode(url); 
        }
    }
}