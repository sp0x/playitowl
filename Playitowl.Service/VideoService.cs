using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.ModelBinding;
using Playitowl.Data.Repositories;
using Playitowl.Models;
using ytDownloader.Extraction;

namespace Playitowl.Service
{
    public class VideoService : IVideoService
    {
        private ModelStateDictionary _modelState;
        private IVideoRepository _videoRepository;

        public VideoService(IVideoRepository videoRepo)
        {
            _videoRepository = videoRepo;
        }
        public VideoService(ModelStateDictionary modelState, IVideoRepository videoRepo) : this(videoRepo)
        {
            _modelState = modelState;
        }

        protected bool ValidateVideo(Video videoToValidate)
        {
            if (_modelState != null)
            {
                if (videoToValidate.Name.Trim().Length == 0)
                    _modelState.AddModelError("Name", "Name is required.");
                if (videoToValidate.Description.Trim().Length == 0)
                    _modelState.AddModelError("Description", "Description is required.");
                //            if (productToValidate.UnitsInStock < 0)
                //                _modelState.AddError("UnitsInStock", "Units in stock cannot be less than zero.");
                return _modelState.IsValid;
            }
            return true;
        }

//        public bool CreateProduct(YtVideo productToCreate)
//        {
//            // Validation logic
//            if (!ValidateProduct(productToCreate))
//                return false;
//
//            // Database logic
//            try
//            {
//                _videoRepository.CreateVideo(productToCreate);
//            }
//            catch
//            {
//                return false;
//            }
//            return true;
//        } 

        public IEnumerable<Video> ListVideos()
        {
            throw new NotImplementedException();
        }

        public void SavePlaylist(YtPlaylistEnumerable playlistEnumerable)
        {
            //save this playlist
            //VideoProvider.PersistAll(model.Videos.ToList());
        }

        public void SaveVideo(Video yvideo)
        {
            var existingVideo = GetById(yvideo.Id); 
            _videoRepository.Create(yvideo);
            
        }

        public Video GetById(string id)
        {
            return _videoRepository.GetById(id);
        }

        public Video Resolve(string idOrUrl)
        {
            YtVideo baseVideo = null;
            string targetLink;
            YtUrlDecoder.PopulateVideoInfo(idOrUrl, ref baseVideo);
            if ((idOrUrl.Contains("http://") | idOrUrl.Contains("https://")))
                targetLink = idOrUrl; 
            bool isPlaylist = YtPlaylistEnumerable.IsPlaylist(idOrUrl);
            var infoUrl = baseVideo.GetInfoUrl();
            //TODO: Improve this to avoid injections of arbitrary urls or ids
            //		If idOrUrl.Contains("http://") OrElse idOrUrl.Contains("https://") AndAlso idOrUrl.Contains("youtube.com") Then
            //			 idOrUrl = YtVideo.GetVideoId(idOrUrl)
            //		End If
            var req = WebRequest.Create(infoUrl);
            HttpWebResponse res = req.GetResponse() as HttpWebResponse;
            StreamReader sr = new StreamReader(res.GetResponseStream());
            string info = sr.ReadToEnd();
            if (!string.IsNullOrEmpty(info))
            {
                var infoSplits = info.Split("&".ToCharArray()).ToDictionary(
                    row =>
                    {
                        var eqsplit = row.Split("=".ToCharArray());
                        return eqsplit[0];
                    }, row =>
                    {
                        var eqsplit = row.Split("=".ToCharArray());
                        if (eqsplit.Length == 1) eqsplit = new string[] { eqsplit[0], "" };
                        return eqsplit[1];

                    });
                if (infoSplits.ContainsKey("title"))
                {
                    baseVideo.Name = System.Web.HttpUtility.UrlDecode(infoSplits["title"]);
                }
                if (infoSplits.ContainsKey("author"))
                {
                    baseVideo.Author = System.Web.HttpUtility.UrlDecode(infoSplits["author"]);
                }
                if (infoSplits.ContainsKey("timestamp"))
                {
                    DateTime time = DateTime.Now;
                    if (DateTime.TryParse(infoSplits["timestamp"], out time))
                    {
                        baseVideo.Added = time;
                    }
                    else
                    {
                        baseVideo.Added = DateTime.Now;
                    }
                }
                if (infoSplits.ContainsKey("length_seconds"))
                {
                    int duration = 0;
                    if (int.TryParse(infoSplits["length_seconds"], out duration)) baseVideo.Duration = duration;

                }

            }
            var videoModel = new Video(baseVideo);
            SaveVideo(videoModel);
            return videoModel;
        }

        public void Remove(string id)
        {
            _videoRepository.Remove(id);
        }

        public IEnumerable<Video> GetSorted(string sortOrder)
        {
            IEnumerable<Video> videos = null;
            switch (sortOrder)
            {
                case "name_desc":
                    videos = _videoRepository.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    videos = _videoRepository.OrderBy(s => s.Added);
                    break;
                case "date_desc":
                    videos = _videoRepository.OrderByDescending(s => s.Added);
                    break;
                default:
                    videos = _videoRepository.OrderBy(s => s.Name);
                    break;
            }
            return videos;
        }
    }
}