﻿// Write your Javascript code.
$(document).ready(function () {
    window.Site = new Site();
    Site.setupHandlers();

    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            console.log('Logged in.');
        }
        else {
            FB.login();
        }
    });
});




function Site() {
    var context = {
        videos: [],
        currentVideoUrl: null,
        currentVideoLoading : {
            id : null
        },
    }
    var controls = {
        btnDownload: $('.index .btnDownload'),
        fldUrl: $('.main-video-input'),
        videosContainer: $('.row.targetVideos ul'),
        Sidebar : new Sidebar(),
    }
    var s = this;
    

    /**
     * 
     * @param {} options 
     * @param {} callback 
     * @returns {} 
     */
    this.prepareVideo = function(options , callback) {
        var vp = new VideoParser(context.currentVideoUrl); 
        var id = getVideoId(context.currentVideoUrl); 
         
        //Get video info and ui element
        var info = vp.parse(function (videoElement) {
            videoElement = $(videoElement);
            var uid = Math.random();
            videoElement.attr('uid', uid);
            videoElement = $('<li class="col-sm-12" ></li>').append(videoElement);
            controls.videosContainer.append(videoElement); //Append it
             
            var loader = new VideoLoader(context.currentVideoUrl, options, 
                function onConnection() {
                    console.log("Video connection established for " + id);
            });

            if (callback != null) {
                var id = options.id || null;
                callback(loader, id, uid);
            }
        });
    }
     
    /**
     * Gets a video element from the list of loading videos
     * @param {} id  of the video
     * @returns {} 
     */
    this.getVideoRow = function (id, uid) {
        //console.log(id);
        var identifier = '#' + id + ".targetVideo";
        if (uid != null) identifier += "[uid = '" + uid + "']";
        var container = $(identifier);
        var progress = $(container.find("#videoLoad_" + id)); 
        return {
            row: container,
            progress: progress,
            isPlaylist: container.attr("isplaylist") === "True",
        }
    }

    /**
     * Starts the video processing
     * @returns {} 
     */
    this.onSearch = function () {
        var self = s;

        context.currentVideoUrl = controls.fldUrl.val();
        //console.log(context.currentVideoUrl);

        var id = getVideoId(context.currentVideoUrl); 
         
        var videoOnly = $('[name="downloadType"]:checked').val() === 'video';   //Wether we'll be downloading only video

        s.prepareVideo({
            onlyVideo: videoOnly,
            id : id
        }, s.onVideoLoading);
    }

    /**
     * Handles messages recieved from the WS video loader
     * @param {} videoLoader 
     * @returns {} 
     */
    this.onVideoLoading = function videoLoading(videoLoader, videoId, uniqueElementId) {
        //Wether the video is in a playlist
        var isPlaylist = videoLoader.context.isPlaylist;

        videoLoader.onMessage(function(event) {
            if (event == null || event.data == null) return;
            var data = event.data;
            if (typeof data == "string") data = JSON.parse(data); 
            var prepared = videoLoader.context.prepared;
            var videoControls = null;

            //If last operation is successfull and the video loader is prepared, then download the video.
            if (data.success && prepared) {
                videoControls = s.getVideoRow(videoId, uniqueElementId);
                isPlaylist = videoLoader.context.isPlaylist;

                if (videoControls.isPlaylist) {
                    //Video is a playlist, warn the user
                    var ok = confirm("This video is a playlist, are you sure you want to download it?");
                    if (ok) {
                        videoLoader.download(true);
                    }
                } else {
                    videoLoader.download(); //If the video is prepared
                }
            }

            /**
             * Handle video playlist item loading, before the item has started loading!
             */
            if (data.action === "loadingPlaylistEntry" && prepared) { //} && videoControls.isPlaylist) {
                //Store the reply id for the action to continue, in order to sync the UI
                var replyId = data.LinkId;
                data.id = data.id || data.Id;
                var vParser = new VideoParser('https://www.youtube.com/watch?v=' + data.id);
                var oldCopy = $(controls.videosContainer.find("#" + data.id));
                
                oldCopy.remove();

                vParser.parse(function (videoElement) { 
                    videoElement = $('<li class="col-sm-12">' + videoElement + '</li>'); 
                    //Remove any previous copies
                    // oldCopy.remove();
                    controls.videosContainer.append(videoElement);

                    videoLoader.raiseReply(replyId);
                    //videoLoader.download();
                });
            }

            //If a download has been started and the video is prepared
            if (data.action === "ready" && prepared) {
                var newId = data.id;
                var parentVideoControls = isPlaylist ? s.getVideoRow(data.id, null) : s.getVideoRow(data.id, uniqueElementId);
                context.currentVideoLoading.id = newId;
                //Make sure there are no other videos with the same id 

                if (isPlaylist && data.playlistMember) { 
                    $('body').scrollTo(parentVideoControls.row.parent());
                } 

            }

        });

        //Update progress for video
        videoLoader.setOnLoadUpdate(function(loadStatus) {
            var currentId = context.currentVideoLoading.id;
            var videoControls = isPlaylist ? s.getVideoRow(currentId, null) : s.getVideoRow(currentId, uniqueElementId);
            //console.log(id + " - " + currentId);
            var perc = loadStatus.completion;
            if (perc == null) return;
            videoControls.progress.text(perc).attr('value', parseInt(perc));
            //console.log(loadStatus);
        });

        //Video has been processed and available for download.
        videoLoader.setOnFinished(function(info) {
            if (info == null || info.path == null) return;

            var videoControls = isPlaylist ? s.getVideoRow(info.id, null) : s.getVideoRow(info.id, uniqueElementId);
            videoControls.progress.text(100).attr('value', parseInt(100));
            if (videoControls.row != undefined) {
                $('body').scrollTo(videoControls.row);
            } 

            context.videos.push(context.currentVideoLoading); 
            context.currentVideoLoading = {}; //Clear the current video
            var path = info.path;
            var url = path;
            var targetVideoDomElement = $(".targetVideo[id='" + info.id + "']");
            var downloadButton = $("<div class='col-sm-1'><a href='" + info.path + "' class='btn btn-default' target='_blank'>Download</a></div>");
            targetVideoDomElement.append(downloadButton);
            console.log(url, info);
//            if (url.indexOf("/downloads/") === 0) {
//                url = '/Video/' + info.id;
//            } 
            DownloadFile(url);
        });
    };


    
    /**
     * Sets up event handlers for ui elements.
     * @returns {} 
     */
    this.setupHandlers = function() {
        var self = this;
        controls.btnDownload.click(self.onSearch); 
//        controls.fldUrl.bind({
//            paste : function () {
//                var val = controls.fldUrl.val();
//                if (val.length === 0) return;
//                self.onSearch(); 
//            }
//        });

        //Side menu
        var $sidemenu = $('nav.side.main');
        $sidemenu.hide();
        jQuery('.menu.side.main').on('click', self.showSidebar);
    }

    this.showSidebar = function()
    {
        var $sidemenu = $('nav.side.main');
        var $container = $('.body-content');
        // State checker
        if ($container.attr('data-state') === 'neutral') {
            //Show the sidebar
            $sidemenu.show();
            $container.attr('data-state', 'slide-right');
        } else {
            //Hide the sidebar
            $container.attr('data-state', 'neutral');
            $sidemenu.hide();
        }
    }

    /**
     * Offers the content on the url(must be valid MIME-type), for download.
     * @param {} url 
     * @returns {} 
     */
    function DownloadFile(url) {
        //document.getElementById('ifDownload').src = url;
        //window.location = url;
        var finalURL = url;
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/octet-stream");
        //xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.open("GET", finalURL, true);
        xhr.responseType = "arraybuffer";
        xhr.onload = function () {
            var bb = new Blob();
            var res = xhr.response;
            if (res) {
                var byteArray = new Uint8Array(res);
            }
            bb.append(byteArray.buffer);
            var blob = bb.getBlob("application/octet-stream");
            var iframe = document.createElement("iframe");
            iframe.style.display = "none";
            iframe.src = window.webkitURL.createObjectURL(blob);
            document.body.appendChild(iframe);
        };
        xhr.send(null);
        //window.location = url;
    };



    this.DownloadFile = DownloadFile;

}


function getVideoId(videoUrl) {
    var rx_videoId = /(watch\?v=)(.*?)(&|$)/ig;
    //console.log(rx_videoId, videoUrl);
    var matches = rx_videoId.exec(videoUrl);
    //console.log(matches);
    if (matches == null) return {
        error: 'Invalid url!'
    }
    var id = matches[2];
    return id;
}


/**
 * Used for basic info fetching.
 * @param {} url 
 * @returns {} 
 */
function VideoParser(url) {
    this.url = url;
    var self = this;

    this.fromQuery = function()
    {
        //TODO: Implement this
        var vp = new VideoParser(url); return vp;
    }
    
    /**
     * Get video html element
     * @param {} url 
     * @param {} callback 
     * @returns {} 
     */
    this.parse = function (url, callback) {
        if (typeof url == "function") callback = url;
        var id = getVideoId( typeof url == "string" ? url : self.url);

        if (id != null && id.error) throw id.error; 
        var info = $.ajax({
            url: '/Video/GetVideoInfoRes',
            data: {
                id: id,
                url: self.url,
            },
            crossDomain: true,
            type: 'POST',
        }).done(callback);
    }
}

function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

/**
 * Loads a set of videos or a single one. Responsible for the control/sync with the Backend.
 * @param {} videoLink 
 * @param {} options 
 * @param {} onConnected 
 * @returns {} 
 */
var VideoLoader = function(videoLink,options, onConnected) {
    //var username = prompt('Please enter a username:');
    var username = makeid(16);
    var uri = 'ws://localhost:8080/api/TrackDownload?id=' + __userDetails.id + '&username=' + username;
    var self = this;
    this.ws = new WebSocket(uri);
    this.context = {
        connected: false,
        lastSentData: null,
        prepared: false,
        isPlaylist : false,
        downloaded : false,
    }
    this.onMessageCallback = null;
    this.onLoadUpdate = null;
    this.onLoadFinished = null;


    this.ws.onopen = function () {
        //Message which preps the context
        var message = { action: "prepare", id: videoLink };
        if (options) message = $.extend(message, options);
        message.id = videoLink; // override in all cases!

        //Setup backend context with the PREPARE action
        self.send(message);
        self.context.connected = true;
        $('#messages').prepend('<div>Connected.</div>');
        if (onConnected != null) {
            onConnected(self);
        } 
    };


    this.ws.onerror = function (event) {
        $('#messages').prepend('<div>ERROR</div>');
    };
    this.onMessage = function (callback) {
        self.onMessageCallback = callback;
    }
    this.send = function (data) {
        self.context.lastSentData = data;
        if (typeof data == "object") data = JSON.stringify(data);
        self.ws.send(data);
    }
    this.setOnLoadUpdate = function(callback) {
        this.onLoadUpdate = callback;
    }
    this.setOnFinished = function(callback) {
        this.onLoadFinished = callback;
    }

    //Setup message event handling and prepare things for the video
    this.ws.onmessage = function (e) { 
        var data = JSON.parse(e.data); 
        if (data != null) {
            var success = data.success;
            var lastAction = self.lastAction();
            if (success) {
                if (lastAction != null && lastAction === "prepare") {
                    self.context.prepared = true;
                    self.context.isPlaylist = data.isPlaylist;
                }
            }
            //also account for acks
            if ((lastAction == "download" || lastAction == "ack") && (data.action == "update" || data.action == "finished")) {
                var data2 = data;
                //delete data2.action;
                if (self.onLoadUpdate != null && data.action == "update") return self.onLoadUpdate(data2);
                if (self.onLoadFinished != null && data.action == "finished") return self.onLoadFinished(data2);
            }
        }
        if (self.onMessageCallback != null) {
            self.onMessageCallback(e);
        }
    };


    this.lastAction = function() {
        if (self.context.lastSentData != null) {
            var action = self.context.lastSentData;
            if (typeof action == "string") action = JSON.parse(action);
            return self.context.lastSentData.action;
        };
        return null;
    }

    /**
     * Acknoledges the action id, which lets the backend execute it!
     * @param {} replyId 
     * @returns {} 
     */
    this.raiseReply = function (replyId) {
        //Acknoledge the action.
        self.send({ action: "ack", replyId: replyId });
    }

    this.raiseFirstReply = function() {
        self.send({ action: "ack", type : 'first' });
    }
    /**
     * 
     * @param {} playlist 
     * @returns {} 
     */
    this.download = function (playlist) {
        if (playlist == undefined) playlist = false;
        self.send({ action: "download", addPlaylistInfo : playlist }); 
    }


    this.start = function () {
        self.send({ action: "ok" });
    }
}







window.downloadFile = function(sUrl) {
 
    //If in Chrome or Safari - download via virtual link click
    if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;
 
        if (link.download !== undefined){
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
            link.download = fileName;
        }
 
        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click' ,true ,true);
            link.dispatchEvent(e);
            return true;
        }
    }
 
    // Force file download (whether supported by server).
    var query = '';
    window.open(sUrl + query);
}
 
window.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
window.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;