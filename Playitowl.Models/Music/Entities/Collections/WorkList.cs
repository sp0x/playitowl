﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Playitowl.Models.Music.Entities.Collections
{
    [XmlRoot("work-list", Namespace = "http://musicbrainz.org/ns/mmd-2.0#")]
    public class WorkList : MusicalEntityList<Work>
    {
        /// <summary>
        /// Gets or sets the list of works.
        /// </summary>
        [XmlElement("work")]
        public override List<Work> Items { get; set; }

        public override string EntityType => "work-list";
        public override string Id { get; set; }
        public override string Query { get; set; }
        public override Type WrapperType => null;
    }
}
