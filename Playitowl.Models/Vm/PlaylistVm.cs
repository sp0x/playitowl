﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Playitowl.Models.Vm
{
    public class PlaylistVm
    {
        public string Cover { get; set; }
        public string Id { get; set; }
        public string Author { get; set; }
        public string Name { get; set; }
        public IEnumerable<VideoVm> Items { get; set; }
    }
}