using System;

namespace Playitowl.Models.Repl
{
    public abstract class TriggeredAction : ITriggeredAction
    {
        public delegate void ActionDelegate(TriggeredAction action, State state);
        public event ActionDelegate Invoked;
        public virtual Guid TriggerId { get; protected set; }

        public bool ExecuteOnce { get; private set; }
        public TriggeredAction Once(bool executeOnlyOnce)
        {
            this.ExecuteOnce = executeOnlyOnce;
            return this;
        }

        public void Invoke(State state)
        {
            Invoked?.Invoke(this, state);
        }
    }

    public class TriggeredAction<T> : TriggeredAction
    {
        private Trigger mTrigger;
        public Trigger Trigger
        {
            get { return mTrigger; }
            set
            {
                //Save the hash of the trigger, so that it's easily removed later on.
                mTrigger = value;
                TriggerId = mTrigger.Guid;
            }
        }
        public T Target { get; set; }

        public TriggeredAction(bool register = true)
        {
            if (register) RepLoop.Factory.Assign(null, this);
        }

        public TriggeredAction(Trigger t)
        {
            RepLoop.Factory.Assign(t, this);
        }
    }
}