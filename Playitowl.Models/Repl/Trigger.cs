using System;

namespace Playitowl.Models.Repl
{
    public class Trigger
    {
        private Guid mGuid;
        public Func<State, bool> Tester;
        public Guid Guid => mGuid;
        public Trigger(Func<State, bool> tester)
        {
            this.Tester = tester;
            mGuid = Guid.NewGuid();
        }
    }
}